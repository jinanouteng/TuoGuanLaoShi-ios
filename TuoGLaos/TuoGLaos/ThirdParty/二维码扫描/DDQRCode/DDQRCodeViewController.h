

#import <UIKit/UIKit.h>

typedef void(^ScanCompleteBlock)(NSString *url);

@interface DDQRCodeViewController : UIViewController

@property (nonatomic, copy, readonly) NSString *urlString;

- (instancetype)initWithScanCompleteHandler:(ScanCompleteBlock)scanCompleteBlock;

- (void)stopRunning;


@end
