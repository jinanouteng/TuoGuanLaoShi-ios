//
//
//  DDNetworkManager.h
//  HttpTool
//
//  Created by Qin on 16/6/7.
//  Copyright © 2016年 qin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDNetworkObjSingle.h"

typedef void(^RequestSuccessBlock)(id responseObject);

typedef void(^RequestFailureBlock)(id errorObject);

@interface DDNetworkManager : NSObject

DDNetworkObjSingleH;

+ (void)POSTVedio:(NSString *)URLString
       parameters:(id)parameters
constructingBodyWithFormDataArray:(NSArray *)formDataArray
          success:(void (^)( id responseObject))success
          failure:(void (^)( NSError *error))failure;
// 无缓存
+ (void)GET:(NSString *)urlString parameters:(NSDictionary *)parameters success:(RequestSuccessBlock)resposeValue failure:(RequestFailureBlock )failure;

// 有缓存
+ (void)GETCache:(NSString *)urlString parameters:(NSDictionary *)parameters success:(RequestSuccessBlock)resposeValue failure:(RequestFailureBlock )failure;

+ (void)POST:(NSString *)urlString parameters:(NSDictionary *)parameters success:(RequestSuccessBlock)resposeValue failure:(RequestFailureBlock )failure;

+ (void)POSTCache:(NSString *)urlString parameters:(NSDictionary *)parameters success:(RequestSuccessBlock)resposeValue failure:(RequestFailureBlock )failure;
+ (void)POSTs:(NSString *)URLString
   parameters:(id)parameters
constructingBodyWithFormDataArray:(NSArray *)formDataArray
      success:(void (^)(id responseObject))success
      failure:(void (^)(NSError *error))failure;
// 上传图片,可多张上传
+ (void)POST:(NSString *)URLString
  parameters:(id)parameters
constructingBodyWithFormDataArray:(NSArray *)formDataArray
     success:(void (^)( id responseObject))success
     failure:(void (^)( NSError *error))failure;

@end

/*DDNetworkManager使用POST添加文件时使用的文件类*/
@interface FormData : NSObject
/**请求参数名*/
@property (nonatomic, copy, readwrite) NSString *name;
/**保存到服务器的文件名*/
@property (nonatomic, copy, readwrite) NSString *fileName;
/**文件类型*/
@property (nonatomic, copy, readwrite) NSString *mimeType;
/**二进制数据*/
@property (nonatomic, strong, readwrite) NSData *data;

@end

#pragma mark request_sendModel

@interface  DDNetworkManagerDate: NSObject
-(instancetype)initWithUrl:(NSString *)url parameters:(NSDictionary *)parameters;

/**
 post请求数据成功结果
 */
-(void)post_RequestFinshSuccess:(RequestSuccessBlock )success failure:(RequestFailureBlock )failure;

/**
 get请求数据成功结果
 */
-(void)get_RequestFinshSuccess:(RequestSuccessBlock )success failure:(RequestFailureBlock )failure;


/**
 post请求数据成功结果
 */
-(void)postCache_RequestFinshSuccess:(RequestSuccessBlock )success failure:(RequestFailureBlock )failure;

/**
 get请求数据成功结果
 */
-(void)getCache_RequestFinshSuccess:(RequestSuccessBlock )success failure:(RequestFailureBlock )failure;


+ (instancetype)makeUrlIndexmember_login:(NSString *)phone member_pwd:(NSString *)password;

+ (instancetype)makeUrlIndexmember_teacherNews:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id;

+ (instancetype)makeUrlIndexmember_leave_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id num:(NSString*)num page:(NSString*)page;

+ (instancetype)makeUrlIndexmember_leave_detail:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id leave_id:(NSString*)leave_id;

+ (instancetype)makeUrlIndexmember_leave_reply:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id leave_id:(NSString*)leave_id up_status:(NSString*)up_status no_content:(NSString*)content;

+ (instancetype)makeUrlIndexmember_all_ban_student_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id;

+ (instancetype)makeUrlIndexmember_student_homework_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id student_id:(NSString*)student_id;

+ (instancetype)makeUrlIndexmembefudao_teacher_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id;

+ (instancetype)makeUrlIndexmembefudao_make_insert:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id student_id:(NSString*)student_id make_date:(NSString*)make_date remark:(NSString*)remark type:(NSString*)type teacher_id:(NSString*)teacher_id content:(NSString*)content;

+ (instancetype)makeUrlIndexmembehomework_sing_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id search_date:(NSString*)search_date num:(NSString*)num page:(NSString*)page;

+ (instancetype)makeUrlIndexmembehomework_sing_add:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id student_code:(NSString*)student_code;

+ (instancetype)makeUrlhomework_sing_doing:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id  num:(NSString*)num page:(NSString*)page;
+ (instancetype)makeUrlhomework_sing_end:(NSDictionary*)parameters;


+ (instancetype)makeUrlhomework_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id search_date:(NSString*)search_date num:(NSString*)num page:(NSString*)page;

+ (instancetype)makeUrlall_student_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id;

+ (instancetype)app_update:(NSString *)system_type app_code:(NSString *)app_code;
@end

