//
//  DDNetworkManager.m
//  
//
//  Created by Qin on 16/6/7.
//  Copyright © 2016年 秦. All rights reserved.
//

#import "DDNetworkManager.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "DDCache.h"
#import "AppDelegate.h"
@interface DDNetworkManager ()


@end

@implementation DDNetworkManager

DDNetworkObjSingleM;


+(void)loginPOST {
    AFHTTPSessionManager *manager = [self AFNetManager];
    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    [securityPolicy setAllowInvalidCertificates:NO];
    manager.securityPolicy = securityPolicy;
}

+ (void)POSTVedio:(NSString *)URLString
       parameters:(id)parameters
constructingBodyWithFormDataArray:(NSArray *)formDataArray
          success:(void (^)( id responseObject))success
          failure:(void (^)( NSError *error))failure{
    [self starInfocatorVisible];

    [[self AFNetManager] POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *data = formDataArray[0];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        // 设置时间格式
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dateString = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString  stringWithFormat:@"%@.jpg", dateString];
        
        [formData appendPartWithFileData:data name:@"file" fileName:fileName mimeType:@"image/jpeg"]; //
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self stopIndicatorVisible];
        success(responseObject);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
        [self stopIndicatorVisible];

    }];
    
}




+ (void)GET:(NSString *)urlString parameters:(NSDictionary *)parameters success:(RequestSuccessBlock)resposeValue failure:(RequestFailureBlock )failure{

    [self starInfocatorVisible];
    
    [[self AFNetManager] GET:urlString
                         parameters:parameters
                         progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (resposeValue) {
            resposeValue(responseObject);
        }
        [self stopIndicatorVisible];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
             failure(error.localizedDescription);
        }
        [self showError:error.localizedDescription];
    }];
}

+ (void)GETCache:(NSString *)urlString parameters:(NSDictionary *)parameters success:(void (^)(id))resposeValue failure:(RequestFailureBlock )failure{
    [self starInfocatorVisible];
    [[self AFNetManager] GET:urlString
                         parameters:parameters
                         progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (resposeValue) {
            resposeValue(responseObject);
        }
        [DDCache qsh_saveDataCache:responseObject forKey:urlString];
        [self stopIndicatorVisible];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        resposeValue([DDCache qsh_ReadCache:urlString]);
        if (failure) {
            failure(error.localizedDescription);
        }
        [self showError:error.localizedDescription];
    }];
}

+ (void)POST:(NSString *)urlString parameters:(NSDictionary *)parameters success:(RequestSuccessBlock)resposeValue failure:(RequestFailureBlock )failure{
    [self starInfocatorVisible];
    [[self AFNetManager] POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self stopIndicatorVisible];
       

        if (resposeValue) {
            resposeValue(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self showError:error.localizedDescription];
        if (failure) {
            failure(error.localizedDescription);
        }
    }];
}

+ (void)POSTCache:(NSString *)urlString parameters:(NSDictionary *)parameters success:(void (^)(id))resposeValue failure:(RequestFailureBlock )failure {
    [self starInfocatorVisible];
    [[self AFNetManager] POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (resposeValue) {
            resposeValue(responseObject);
        }
        [DDCache qsh_saveDataCache:responseObject forKey:urlString];
        [self stopIndicatorVisible];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        resposeValue([DDCache qsh_ReadCache:urlString]);
        if (failure) {
            failure(error.localizedDescription);
        }
        [self showError:error.localizedDescription];
    }];
}

+ (void)POST:(NSString *)URLString
        parameters:(id)parameters
constructingBodyWithFormDataArray:(NSArray<FormData *> *)formDataArray
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSError *error))failure
{
    [[self AFNetManager] POST:URLString
                         parameters:parameters
                         constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (FormData * data in formDataArray) {
            [formData appendPartWithFileData:data.data
                      name:data.name
                      fileName:data.fileName
                      mimeType:data.mimeType];
            
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"%lf",1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //   [MBProgressHUD showMessage:error.localizedDescription];
        
        failure(error);
    }];
}

+ (void)POSTs:(NSString *)URLString
  parameters:(id)parameters
constructingBodyWithFormDataArray:(NSArray *)formDataArray
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSError *error))failure
{
    [[self AFNetManager] POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < formDataArray.count; i++) {
            UIImage *image = formDataArray[i];
            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
            
            FormData *data = [[FormData alloc] init];
            data.name = @"avatar";
            data.data = imageData;
            data.mimeType = @"image/jpg";
            data.fileName = @"headicon.jpg";
            
            [formData appendPartWithFileData:data.data
                                        name:data.name
                                    fileName:data.fileName
                                    mimeType:data.mimeType];
//            // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
//            // 要解决此问题，
//            // 可以在上传时使用当前的系统事件作为文件名
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            // 设置时间格式
//            [formatter setDateFormat:@"yyyyMMddHHmmss"];
//            NSString *dateString = [formatter stringFromDate:[NSDate date]];
//            NSString *fileName = [NSString  stringWithFormat:@"%@%d.jpg", dateString,i];
//            /*
//             *该方法的参数
//             1. appendPartWithFileData：要上传的照片[二进制流]
//             2. name：对应网站上[upload.php中]处理文件的字段（比如upload）
//             3. fileName：要保存在服务器上的文件名
//             4. mimeType：上传的文件的类型
//             */
//            [formData appendPartWithFileData:imageData name:@"Upload" fileName:fileName mimeType:@"image/jpeg"]; //
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
//    
//    [[self AFNetManager] POST:[NSString stringWithFormat:@"%@%@",kRequestHeaderUrl,URLString] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        //        for (FormData * data in formDataArray) {
//        //            [formData appendPartWithFileData:data.data
//        //                                        name:data.name
//        //                                    fileName:data.fileName
//        //                                    mimeType:data.mimeType];
//        //        }
//        // formData: 专门用于拼接需要上传的数据,在此位置生成一个要上传的数据体
//        // 这里的_photoArr是你存放图片的数组
//        for (int i = 0; i < formDataArray.count; i++) {
//            
//            UIImage *image = formDataArray[i];
//            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
//            // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
//            // 要解决此问题，
//            // 可以在上传时使用当前的系统事件作为文件名
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            // 设置时间格式
//            [formatter setDateFormat:@"yyyyMMddHHmmss"];
//            NSString *dateString = [formatter stringFromDate:[NSDate date]];
//            NSString *fileName = [NSString  stringWithFormat:@"%@%d.jpg", dateString,i];
//            /*
//             *该方法的参数
//             1. appendPartWithFileData：要上传的照片[二进制流]
//             2. name：对应网站上[upload.php中]处理文件的字段（比如upload）
//             3. fileName：要保存在服务器上的文件名
//             4. mimeType：上传的文件的类型
//             */
//            [formData appendPartWithFileData:imageData name:@"Upload" fileName:fileName mimeType:@"image/jpeg"]; //
//        }
//    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        success(responseObject);
//        
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        failure(error);
//    }];
}

/**
 *  公用一个AFHTTPSessionManager
 *
 *  @return AFHTTPSessionManager
 */
+ (AFHTTPSessionManager *)AFNetManager
{
    static AFHTTPSessionManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [AFHTTPSessionManager manager];
        //设置请求的超时时间
//        manager.requestSerializer.timeoutInterval = 20;
//        manager.securityPolicy = [self customSecurityPolicy];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/*", nil];
    });
    return manager;
}

// SSL认证
+ (AFSecurityPolicy*)customSecurityPolicy

{
    // /先导入证书,证书导入项目中
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"YourCertificate" ofType:@"cer"];//证书的路径
    
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    
    // AFSSLPinningModeCertificate 使用证书验证模式
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    // allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    
    // 如果是需要验证自建证书，需要设置为YES
    
    securityPolicy.allowInvalidCertificates = YES;
    
    //validatesDomainName 是否需要验证域名，默认为YES；
    
    //假如证书的域名与你请求的域名不一致，需把该项设置为NO；如设成NO的话，即服务器使用其他可信任机构颁发的证书，也可以建立连接，这个非常危险，建议打开。
    
    //置为NO，主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
    
    //如置为NO，建议自己添加对应域名的校验逻辑。
    
    securityPolicy.validatesDomainName = NO;
    
    securityPolicy.pinnedCertificates = [NSSet setWithObjects:certData, nil];// @[certData];
    
    return securityPolicy;
    
}

#pragma mark - 网络加载相关

+ (void)starInfocatorVisible {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    [SVProgressHUD setFont:[UIFont systemFontOfSize:12]];
    [SVProgressHUD showWithStatus:@"正在加载"];
}

+ (void)stopIndicatorVisible {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [SVProgressHUD dismiss];

}

+ (void)showError:(NSString *)errorString {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [SVProgressHUD showErrorWithStatus:errorString];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
}
@end

#pragma mark request_sendModel

@interface DDNetworkManagerDate ()

@property(nonatomic,copy,readonly)NSString *url;
@property(nonatomic,strong,readonly)NSDictionary *parameters;

@end

@implementation DDNetworkManagerDate

+(instancetype)initWithUrl:(NSString *)url parameters:(NSDictionary *)parameters{
    return [[self alloc] initWithUrl:url parameters:parameters];
}

-(instancetype)initWithUrl:(NSString *)url parameters:(NSDictionary *)parameters{
    if (self = [super init]) {
        self.url = url;
        self.parameters = parameters;
    }
    return self;
}

-(void)setUrl:(NSString *)url{
    _url = url;
}

-(void)setParameters:(NSDictionary *)parameters{
    NSMutableDictionary *muparameters =(parameters)?parameters.mutableCopy:@{}.mutableCopy;
    //([DDUserManager sharedInstance].isLogin)?[muparameters setValue:[DDUserManager sharedInstance].userToken forKey:@"token"]:muparameters;
    NSArray *allKey = [muparameters allKeys];
    NSArray *values = [muparameters allValues];
    __block NSString *urlStr = @"";
    
    [allKey enumerateObjectsUsingBlock:^(NSString  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *value = (idx < values.count)?value = values[idx]:@"";
        urlStr = [urlStr stringByAppendingFormat:@"&%@=%@",obj,value];
    }];
    
#ifdef DEBUG
    DLog(@"\n\n\n\n*************\n*****************\n%@%@%@?%@\n***************\n***************\n\n\n\n",kRequestHeaderUrl,@"",_url,urlStr);
#else
    
#endif
    _parameters = muparameters.copy;
    _url = [NSString stringWithFormat:@"%@%@",kRequestHeaderUrl,_url];
}

/**
 请求数据成功结果
 */
-(void)post_RequestFinshSuccess:(RequestSuccessBlock)success failure:(RequestFailureBlock )failure{
    [DDNetworkManager POST:self.url parameters:self.parameters success:success failure:failure];
}

/**
 get请求数据成功结果
 */
-(void)get_RequestFinshSuccess:(RequestSuccessBlock )success failure:(RequestFailureBlock )failure{
    [DDNetworkManager GET:self.url parameters:self.parameters success:success failure:failure];
}

/**
 post缓存 请求数据成功结果
 */
-(void)postCache_RequestFinshSuccess:(RequestSuccessBlock )success failure:(RequestFailureBlock )failure{
    [DDNetworkManager POSTCache:self.url parameters:self.parameters success:success failure:failure];
}

/**
 get 缓存 请求数据成功结果
 */
-(void)getCache_RequestFinshSuccess:(RequestSuccessBlock )success failure:(RequestFailureBlock )failure{
    [DDNetworkManager GETCache:self.url parameters:self.parameters success:success failure:failure];
}
//登录：ok
//url:http://sysbxuqb.cnhv6.hostpod.cn/api/index/teacher_login
//
//参数：
//任课老师
//phone:18764421160       //登录账号（手机号码）
//password:222222          //密码
+ (instancetype)makeUrlIndexmember_login:(NSString *)phone member_pwd:(NSString *)password{
    return [DDNetworkManagerDate initWithUrl:@"/api/index/teacher_login" parameters:@{@"phone":phone,@"password":password}];
}
//版本更新
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/app_update
//
//参数：
//system_type:1             //系统类型 1.android 2.iOS
//app_code:1                        //版本对比号
+ (instancetype)app_update:(NSString *)system_type app_code:(NSString *)app_code{
       return [DDNetworkManagerDate initWithUrl:@"/Api/index/app_update" parameters:@{@"system_type":system_type,@"app_code":app_code}];
}
//老师详情(任课老师，班主任，学管师)：ok
//url:http://sysbxuqb.cnhv6.hostpod.cn/api/index/teacher_detail
//
//参数
//token:bfda5bd771c805b992aec10ac382ee66                  //登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:15                               //登录接口返回的login_id

+ (instancetype)makeUrlIndexmember_teacherNews:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id{
    return [DDNetworkManagerDate initWithUrl:@"/api/index/teacher_detail" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id}];
}
//请假列表（班主任）ok
//url：http://sysbxuqb.cnhv6.hostpod.cn/Api/index/leave_list
//
//参数：
//token:34089fcb077046965e5a5478f139caed//登录接口返回的token
//school_id:43                               //登录接口返回的school_id
//login_id:30                               //登录接口返回的login_id
//num:10                     //一页条数，可留空，默认为10
//page:1                     //页数，可留空，默认为1
+(instancetype)makeUrlIndexmember_leave_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id num:(NSString *)num page:(NSString *)page{
    return [DDNetworkManagerDate initWithUrl:@"/Api/index/leave_list" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"num":num,@"page":page}];
}
//url：http://sysbxuqb.cnhv6.hostpod.cn/Api/index/leave_detail
//
//参数：
//token:34089fcb077046965e5a5478f139caed//登录接口返回的token
//school_id:43                               //登录接口返回的school_id
//login_id:30                               //登录接口返回的login_id
//leave_id                               //请假id
+ (instancetype)makeUrlIndexmember_leave_detail:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id leave_id:(NSString*)leave_id{
     return [DDNetworkManagerDate initWithUrl:@"/Api/index/leave_detail" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"leave_id":leave_id}];
}

//请假详情（班主任）
//url：http://sysbxuqb.cnhv6.hostpod.cn/Api/index/leave_reply
//参数：
//token:34089fcb077046965e5a5478f139caed//登录接口返回的token
//school_id:43                               //登录接口返回的school_id
//login_id:30                               //登录接口返回的login_id
//leave_id                               //请假id
//up_status                              //审核状态 2同意 3不同意
//no_content                               //审核理由，不必须
+ (instancetype)makeUrlIndexmember_leave_reply:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id leave_id:(NSString*)leave_id up_status:(NSString*)up_status no_content:(NSString*)content{
    return [DDNetworkManagerDate initWithUrl:@"/Api/index/leave_reply" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"leave_id":leave_id,@"up_status":up_status,@"content":content}];
}
//学生列表（班主任所属接口）ok
//url:http://sysbxuqb.cnhv6.hostpod.cn/api/index/all_ban_student_list
//
//参数(Post)
//token:2ec04ee0e86260e05feee313e9ea5355                 //登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:17                               //登录接口返回的login_id

+ (instancetype)makeUrlIndexmember_all_ban_student_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id{
   
    return [DDNetworkManagerDate initWithUrl:@"/api/index/all_ban_student_list" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id}];
}
//作业列表（班主任）ok
//
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/student_homework_list
//
//参数(GET)
//token:34089fcb077046965e5a5478f139caed//登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:17                               //登录接口返回的login_id
//student_id:1                             //学生id

+ (instancetype)makeUrlIndexmember_student_homework_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id student_id:(NSString*)student_id{
    return [DDNetworkManagerDate initWithUrl:@"/Api/index/student_homework_list" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"student_id":student_id}];
}
//辅导老师列表，用于班主任为学生补签时选择老师（班主任）ok
//
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/fudao_teacher_list
//
//参数
//token:34089fcb077046965e5a5478f139caed//登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:17                               //登录接口返回的login_id
+ (instancetype)makeUrlIndexmembefudao_teacher_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id{
    return [DDNetworkManagerDate initWithUrl:@"/Api/index/fudao_teacher_list" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id}];
}

//辅导补签（班主任）ok
//
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/fudao_make_insert
//
//参数
//token:34089fcb077046965e5a5478f139caed//登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:17                               //登录接口返回的login_id
//student_id:1                             //学生id
//make_date：2017-10-16                                    //补签日期
//remark:test                    //补签备注
//type:1                   //补签类型 1签到补签 2签退补签
//teacher_id:1                   //辅导老师id
//content:test                    //作业详情，可留空
+ (instancetype)makeUrlIndexmembefudao_make_insert:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id student_id:(NSString*)student_id make_date:(NSString*)make_date remark:(NSString*)remark type:(NSString*)type teacher_id:(NSString*)teacher_id content:(NSString*)content{
    return [DDNetworkManagerDate initWithUrl:@"/Api/index/fudao_make_insert" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"student_id":student_id,@"make_date":make_date,@"remark":remark,@"type":type,@"teacher_id":teacher_id,@"content":content}];
}

//学生辅导签到列表（任课老师）：ok
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/homework_sing_list
//
//参数：
//token:bfda5bd771c805b992aec10ac382ee66                  //登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:15                               //登录接口返回的login_id
//search_date：2017-10-13                                     //查询日期，可留空
//num:10                     //一页条数，可留空，默认为10
//page:1                     //页数，可留空，默认为1

+ (instancetype)makeUrlIndexmembehomework_sing_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id search_date:(NSString*)search_date num:(NSString*)num page:(NSString*)page{
     return [DDNetworkManagerDate initWithUrl:@"/Api/index/homework_sing_list" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"search_date":search_date,@"num":num,@"page":page}];
}
//学生辅导签到(任课老师) OK
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/homework_sing_add
//
//参数
//token:bfda5bd771c805b992aec10ac382ee66                  //登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:15                               //登录接口返回的login_id
//student_code:ZH32_00001                           //扫码所得
+ (instancetype)makeUrlIndexmembehomework_sing_add:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id student_code:(NSString*)student_code{
    return [DDNetworkManagerDate initWithUrl:@"/Api/index/homework_sing_add" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"student_code":student_code}];
}



+ (instancetype)makeUrlhomework_sing_doing:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id  num:(NSString*)num page:(NSString*)page{
     return [DDNetworkManagerDate initWithUrl:@"/Api/index/homework_sing_doing" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"num":num,@"page":page}];
}
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/homework_sing_end
//参数:
//token:bfda5bd771c805b992aec10ac382ee66                  //登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:15                               //登录接口返回的login_id
//student_id:1                               //学生id
//content:test                              //作业内容
//homeword_pic                               //图集

+ (instancetype)makeUrlhomework_sing_end:(NSDictionary*)parameters{
    return [DDNetworkManagerDate initWithUrl:@"/Api/index/homework_sing_end" parameters:parameters];
}
//作业列表（任课老师）：ok
//url:http://sysbxuqb.cnhv6.hostpod.cn/Api/index/homework_list
//
//
//参数：
//token:bfda5bd771c805b992aec10ac382ee66                  //登录接口返回的token
//school_id:32                               //登录接口返回的school_id
//login_id:15                               //登录接口返回的login_id
//search_date：2017-10-13                                     //查询日期，可留空
//num:10                     //一页条数，可留空，默认为10
//page:1                     //页数，可留空，默认为1
+ (instancetype)makeUrlhomework_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id search_date:(NSString*)search_date num:(NSString*)num page:(NSString*)page{
     return [DDNetworkManagerDate initWithUrl:@"/Api/index/homework_list" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id,@"search_date":search_date,@"num":num,@"page":page}];
}

+ (instancetype)makeUrlall_student_list:(NSString *)token school_id:(NSString *)school_id login_id:(NSString *)login_id{
    return [DDNetworkManagerDate initWithUrl:@"/api/index/all_student_list" parameters:@{@"token":token,@"school_id":school_id,@"login_id":login_id}];
}
@end

@implementation FormData
@end

