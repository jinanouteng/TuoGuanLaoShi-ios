

#import "UserModel.h"

@implementation UserModel
+ (UserModel *)shareInstance
{
    static UserModel *usermodel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        usermodel = [[UserModel alloc] init];
    });
    return usermodel;
}
@end
