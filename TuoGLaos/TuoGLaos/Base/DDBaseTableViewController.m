
#import "DDBaseTableViewController.h"
#import "DDBaseTableViewCell.h"

@interface DDBaseTableViewController ()

@end

@implementation DDBaseTableViewController
-(NSMutableArray *)resultList{
    if (_resultList == nil) {
        _resultList = @[].mutableCopy;
    }
    return _resultList;
}


-(UITableView *)tableView{
    if(_tableView == nil){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth , kScreenHeight) style:self.tableViewStyle];
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[DDBaseTableViewCell class] forCellReuseIdentifier:[DDBaseTableViewCell cellReuseIdentifier]];
    [self.tableView.mj_header beginRefreshing];
}

- (void)setPageControllType:(TPageControllType)pageControllType{
    
    _pageControllType = pageControllType;
    switch (pageControllType) {
        case EPageControllAll :{
            [self refreshAction];
            [self initLoadingMore];
            break;
        }
        case EPageControllPullRefresh:{
            [self refreshAction];
            break;
        }
        case EPageControllLoadMore :
            [self initLoadingMore];
            break;
        default:
            break;
    }
}

-(void)initLoadingMore{
    //    @weakify(self);
    //    [RACObserve(self.tableView, contentOffset) subscribeNext:^(id x) {
    //        @strongify(self);
    //        if ([self canLoadMore]) {
    //            self.tableView.tableFooterView = self.loadMoreView;
    //            [self.loadMoreView startAnimation];
    //            [self loadMoreData];
    //        }
    //    }];
    @weakify(self);
    self.tableView.mj_footer  = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadMoreData];
    }];
}

- (void)loadMoreData
{
    
}

-(void)refreshAction{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self startRefresh];
    }];
}

-(void)startRefresh{
    
}

-(void)endRefreshing{
    [self.tableView.mj_header endRefreshing];
}


-(void)endLoadingMore{
    [self.tableView.mj_footer endRefreshing];
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _resultList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DDBaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[DDBaseTableViewCell cellReuseIdentifier] forIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 0.00f;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
