

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface DDBaseTableViewCell : UITableViewCell

-(void)__setData:(id)data;

+(NSString *)cellReuseIdentifier;

@end
