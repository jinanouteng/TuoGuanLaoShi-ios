
#import "BaseNavigationC.h"
@interface BaseNavigationC ()

@end

@implementation BaseNavigationC

- (void)viewDidLoad {
    [super viewDidLoad];

}

//push时隐藏tabbar
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.childViewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
