

#import "DDBaseViewController.h"


typedef NS_ENUM(NSUInteger, TPageControllType) {
    EPageControllNone,
    EPageControllPullRefresh,
    EPageControllLoadMore,
    EPageControllAll
};


@interface DDBaseTableViewController : DDBaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (assign, nonatomic) TPageControllType pageControllType;
@property(nonatomic,assign)UITableViewStyle tableViewStyle;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *resultList;

-(void)refreshAction;

-(void)startRefresh;

-(void)endRefreshing;

-(void)endLoadingMore;

- (void)loadMoreData;

@end
