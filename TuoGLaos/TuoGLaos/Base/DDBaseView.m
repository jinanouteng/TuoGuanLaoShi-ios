

#import "DDBaseView.h"

@implementation DDBaseView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
}

//类名和xib文件名 一样才能使用此方法
+(id )initWithNibItemIndex:(NSInteger)index{
    return [[NSBundle mainBundle]loadNibNamed:NSStringFromClass(self) owner:nil options:nil][index];
}
@end
