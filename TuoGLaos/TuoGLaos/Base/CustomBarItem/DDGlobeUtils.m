//
//  DDGlobeUtils.m
//  DDUP
//
//  Created by iOS on 17/1/21.
//  Copyright © 2017年 IOS. All rights reserved.
//

#import "DDGlobeUtils.h"


@implementation DDGlobeUtils

+ (CGSize) sizeWithFont:(UIFont *)font text:(NSString *)string constraintSize:(CGSize)constraintSize{
    CGSize size = [string boundingRectWithSize:constraintSize options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:[NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,nil] context:nil].size;
    size = CGSizeMake(size.width +3, size.height + 4.f);
    return size;
}

//设置不同字体颜色大小
+ (NSAttributedString *)setDifferentFontColorsWithString:(NSString *)string font:(UIFont *)font range:(NSRange)range color:(UIColor *)vaColor{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    [str addAttribute:NSForegroundColorAttributeName value:vaColor range:range];
    
    [str addAttribute:NSFontAttributeName value:font range:range];
    return str;
}

+(NSString *)stringWithHtmlContent:(NSString *)content{
    static NSString const *kHtmlLyoutStr = @"<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"><style>img{max-width: 100%; width:auto; height:auto;}</style></head></body>";
 return  [NSString stringWithFormat:@"%@%@</body></html>",kHtmlLyoutStr,content];
}
/** 取消searchBar背景色 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(void)cornerRadiusWithView:(UIView *)view radius:(CGFloat )radius{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                        cornerRadius:radius];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}


@end
