//
//  DDGlobeUtils.h
//  DDUP
//
//  Created by iOS on 17/1/21.
//  Copyright © 2017年 IOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DDGlobeUtils : NSObject

/**
 获取字符串尺寸

 @param font 字体大小
 @param string 空间大小
 @param constraintSize 内容
 @return 返回尺寸
 */
+ (CGSize) sizeWithFont:(UIFont *)font text:(NSString *)string constraintSize:(CGSize)constraintSize;

+ (NSAttributedString *)setDifferentFontColorsWithString:(NSString *)string font:(UIFont *)font range:(NSRange)range color:(UIColor *)vaColor;

/**
 加载html
 
 @param content 内容
 @return string
 */

+(NSString *)stringWithHtmlContent:(NSString *)content;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

+(void)cornerRadiusWithView:(UIView *)view radius:(CGFloat )radius;

@end
