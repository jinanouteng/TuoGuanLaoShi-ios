
#import "DDBaseViewController.h"
#import "XHToast.h"
#import "IQKeyboardManager.h"
#import "SVProgressHUD.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import <NSDictionary+YYAdd.h>
#import "MainTabBarController.h"
@interface DDBaseViewController ()

@end

@implementation DDBaseViewController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorHex(0xF4F2F3);
//    [self makeNavBar];
    if (!self.isTabVC) {
        [self addNavReturnBtn];
    }
    //    if (!IOS9_OR_LATER) {
    //        self.automaticallyAdjustsScrollViewInsets = NO;
    //    }
   
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [SVProgressHUD dismiss];
}
- (void)makeNavBar{
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.translucent = NO;
    
}

- (void)addNavReturnBtn{
    
    CustomBarItem *left1 = [CustomBarItem itemWithImage:@"back" size:CGSizeMake(20, 20) type:left];
//    [left1 setOffset:-10];
    [left1 addTarget:self selector:@selector(returnToPrevious) event:UIControlEventTouchUpInside];
    NSMutableArray *items = [NSMutableArray array];
    [items addObjectsFromArray:left1.items];
    self.navigationItem.leftBarButtonItems = items;
//    UIButton * titleView = [[UIButton alloc]initWithFrame:CGRectMake(-12, 0, 35, 20)];
//    titleView.clipsToBounds = NO;
//    [titleView addTarget:self action:@selector(returnToPrevious) forControlEvents:UIControlEventTouchUpInside];
//    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(-6, 0, titleView.width, titleView.height)];
//    titleLabel.text = NSLocalizedString(@"返回", nil) ;
//    titleLabel.font = [UIFont systemFontOfSize:REAL_LEN(48.f)];
//    titleLabel.textColor = [UIColor colorWithHexString:@"#919191"];
//    titleLabel.textColor = [UIColor whiteColor];
//    [titleView setImage:[UIImage imageNamed:@"title_back"] forState:(UIControlStateNormal)];
//    [titleView addSubview:titleLabel];
    
//    UIBarButtonItem * titleItem = [[UIBarButtonItem alloc] initWithCustomView:titleView];
}

- (void)returnToPrevious{
    if(_returnType == EReturn_pop){
        [self.navigationController popViewControllerAnimated:YES];
    }else if (_returnType == EReturn_dismiss){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }else if (_returnType == EReturn_Main){
        MainTabBarController *main = (MainTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        main.selectedIndex = 0;
    }
}

- (void)moreOption
{
    
    
}

- (void)addRightNavWithTitle:(NSString *)title{
    
    CustomBarItem *item = [self.navigationItem setItemWithTitle:title titleColor:[UIColor whiteColor] fontSize:REAL_LEN(48.f) itemType:right];
    [item addTarget:self selector:@selector(moreOption) event:UIControlEventTouchUpInside];
}

- (void)addRightNavWithCustomImage:(NSString *)imageName{
    CustomBarItem *item = [self.navigationItem setItemWithImage:imageName size:CGSizeMake(REAL_LEN_375(40), REAL_LEN_375(40)) itemType:right];
    [item addTarget:self selector:@selector(moreOption) event:UIControlEventTouchUpInside];
}


-(void)showTopMessage:(NSString *)message{
    [XHToast showTopWithText:message];
}
-(void)showCenterMessage:(NSString *)message{
    [XHToast showCenterWithText:message];
}
-(void)showBottomMessag:(NSString *)message{
    [XHToast showBottomWithText:message];
}

-(void)showSuccessMessage:(NSString *)message{
    [SVProgressHUD showSuccessWithStatus:message];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
}

-(void)showErorrMessage:(NSString *)message{
    [SVProgressHUD showErrorWithStatus:message];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
}

-(void)showLoadingMessage:(NSString *)message{
    [SVProgressHUD showWithStatus:message];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
}

- (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font maxSize:(CGSize)maxSize{
    if (kStringIsEmpty(string)) {
        return CGSizeZero;
    }
    NSDictionary *dict = @{NSFontAttributeName : font};
    
    CGSize size = [string boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil].size;
    
    return size;
}

-(NSInteger)requestFinshCode:(NSDictionary *)responseObject isShowMessage:(BOOL)isShow{
    int stateCode = -1;
    if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary* state = [responseObject objectForKey:@""];
        if (state != nil && [state isKindOfClass:[NSDictionary class]]) {
            stateCode = [state intValueForKey:@"" default:-1];
            NSString *message = [state stringValueForKey:@"" default:@""];
            if (stateCode != 0 && message.length != 0 && isShow) {
                [XHToast showBottomWithText:message];
            }
        }
    }
    
    return stateCode;
}

-(NSDictionary *)dictionaryRequestSuccess:(NSDictionary *)responseObject{
    
//    return [responseObject dictionaryValueForKey:kRequsetDataKey default:@{}];
    return [responseObject objectForKey:@""];
}

-(NSArray *)arrayRequestSuccess:(NSDictionary *)responseObject{
//    return [responseObject arrayValueForKey:kRequsetDataKey default:@[]];
    return [responseObject objectForKey:@""];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationFade;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
