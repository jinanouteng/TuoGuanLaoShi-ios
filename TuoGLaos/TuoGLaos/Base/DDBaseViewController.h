

#import <UIKit/UIKit.h>

typedef enum{
    EReturn_pop = 0,
    EReturn_dismiss,
    EReturn_Main
}TReturnType;

@interface DDBaseViewController : UIViewController
@property(nonatomic, assign)BOOL isTabVC;

@property(nonatomic,assign)BOOL isTabbar;
@property(assign, nonatomic)TReturnType returnType;

- (void)moreOption;
- (void)addRightNavWithTitle:(NSString *)title;
- (void)addRightNavWithCustomImage:(NSString *)imageName;


-(void)showTopMessage:(NSString *)topMessage;
-(void)showCenterMessage:(NSString *)message;
-(void)showBottomMessag:(NSString *)message;

-(void)showSuccessMessage:(NSString *)message;
-(void)showErorrMessage:(NSString *)message;
-(void)showLoadingMessage:(NSString *)message;

- (void)returnToPrevious;//返回上一级页面

-(NSInteger)requestFinshCode:(NSDictionary *)responseObject isShowMessage:(BOOL)isShow;

-(NSDictionary *)dictionaryRequestSuccess:(NSDictionary *)responseObject;
-(NSArray *)arrayRequestSuccess:(NSDictionary *)responseObject;


- (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font maxSize:(CGSize)maxSize;

@property (nonatomic,assign)NSInteger currentPage;
@end
