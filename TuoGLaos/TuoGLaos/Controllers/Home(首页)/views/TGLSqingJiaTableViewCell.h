//
//  TGLSqingJiaTableViewCell.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/25.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGLSHomeModel.h"
@interface TGLSqingJiaTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *studentName;
@property (weak, nonatomic) IBOutlet UILabel *parentsName;
@property (weak, nonatomic) IBOutlet UIView *cellBackView;

@property (weak, nonatomic) IBOutlet UILabel *thingType;
@property (weak, nonatomic) IBOutlet UILabel *thingTime;
@property (weak, nonatomic) IBOutlet UILabel *thingReason;
@property (weak, nonatomic) IBOutlet UILabel *createTime;
@property (weak, nonatomic) IBOutlet UIImageView *thingStaus;
@property (weak, nonatomic) IBOutlet UILabel *thingDayNum;
//@property (weak, nonatomic) IBOutlet UIButton *thingDetaiBtn;
//@property (nonatomic, copy)void (^EnterNextView)(void);
- (void)configWithModel:(TGLSleave_listModel *)model;

@end
