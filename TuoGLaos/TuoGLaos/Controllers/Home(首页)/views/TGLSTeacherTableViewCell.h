//
//  TGLSTeacherTableViewCell.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGLSHomeModel.h"
@interface TGLSTeacherTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *studentNumber;//序号
@property (weak, nonatomic) IBOutlet UILabel *studentCode;//学号
@property (weak, nonatomic) IBOutlet UILabel *studentName;//名字
@property (weak, nonatomic) IBOutlet UIImageView *studentIcon;//头像
- (void)configWithModel:(TGLSStudentList *)model;
@end
