//
//  TGLSTeacherTableViewCell.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSTeacherTableViewCell.h"

@implementation TGLSTeacherTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)configWithModel:(TGLSStudentList *)model{
    self.studentNumber.text = model.ids;
    self.studentCode.text = model.student_code;
    self.studentName.text = model.name;
    self.studentIcon.layer.cornerRadius = 20;
    self.studentIcon.layer.masksToBounds = YES;
    [self.studentIcon sd_setImageWithURL:[NSURL URLWithString:model.headimg] placeholderImage:[UIImage imageNamed:@"学生"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
