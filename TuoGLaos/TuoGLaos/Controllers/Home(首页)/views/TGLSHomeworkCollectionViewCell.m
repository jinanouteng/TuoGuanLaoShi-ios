//
//  TGLSHomeworkCollectionViewCell.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSHomeworkCollectionViewCell.h"

@implementation TGLSHomeworkCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)configWithModel:(TGLSHomeworkListModel *)model{
    self.createTime.text = model.create_time;
    self.homeworkContent.text = model.muban_content;
   
    self.homeworkContent.numberOfLines = 0;
}
@end
