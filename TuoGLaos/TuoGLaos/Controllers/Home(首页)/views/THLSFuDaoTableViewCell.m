//
//  THLSFuDaoTableViewCell.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/27.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "THLSFuDaoTableViewCell.h"

@implementation THLSFuDaoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)configWithModel:(TGLSStudentList *)model{
    self.studentCode.text = [NSString stringWithFormat:@"学号 : %@",model.student_code];
    self.studentName.text = [NSString stringWithFormat:@"姓名 : %@",model.name];
    self.qianDaoTime.text = [NSString stringWithFormat:@"签到时间 : %@",model.create_time];
    self.backView.layer.cornerRadius = 6;
    self.backView.layer.masksToBounds = YES;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
