//
//  TGLSTeacherListTableViewCell.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGLSHomeModel.h"
@interface TGLSTeacherListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *teacherNum;
@property (weak, nonatomic) IBOutlet UILabel *teacherName;
@property (weak, nonatomic) IBOutlet UILabel *teacherPhone;
- (void)configWithModel:(TGLSTeacherListModel *)model;
@end
