//
//  TGLSTeacherListTableViewCell.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSTeacherListTableViewCell.h"

@implementation TGLSTeacherListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)configWithModel:(TGLSTeacherListModel *)model{
    self.teacherName.text = model.name;
    self.teacherPhone.text = model.phone;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
