//
//  TGLSHomeworkCollectionViewCell.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGLSHomeModel.h"
@interface TGLSHomeworkCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *createTime;
@property (weak, nonatomic) IBOutlet UILabel *homeworkContent;
- (void)configWithModel:(TGLSHomeworkListModel *)model;
@end
