//
//  TGLSqingJiaTableViewCell.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/25.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSqingJiaTableViewCell.h"

@implementation TGLSqingJiaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    [[self.thingDetaiBtn rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(id x) {
//        if (self.EnterNextView) {
//            self.EnterNextView();
//        }
//    }];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configWithModel:(TGLSleave_listModel *)model{
    self.cellBackView.layer.cornerRadius = 10;
    self.studentName.text = [NSString stringWithFormat:@"%@(%@)",model.name,model.student_code];
    self.parentsName.text = model.parents_name;
    self.thingType.text = model.type;
    self.createTime.text = model.create_time;
    self.thingDayNum.text = [NSString stringWithFormat:@"%@天",model.day_num];
    self.thingTime.text = [NSString stringWithFormat:@"%@--%@",model.start_time,model.end_time];
    self.thingReason.text = model.content;
    if ([model.status isEqualToString:@"1"]) {
        self.thingStaus.image = [UIImage imageNamed:@"weipi80"];
    }else if([model.status isEqualToString:@"2"]){
        self.thingStaus.image = [UIImage imageNamed:@"yipi80"];
    }else{
        self.thingStaus.image = [UIImage imageNamed:@"jujue80"];
    }
    
}
@end
