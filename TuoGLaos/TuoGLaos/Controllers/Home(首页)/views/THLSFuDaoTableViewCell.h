//
//  THLSFuDaoTableViewCell.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/27.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGLSHomeModel.h"
@interface THLSFuDaoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *studentCode;
@property (weak, nonatomic) IBOutlet UILabel *studentName;
@property (weak, nonatomic) IBOutlet UILabel *qianDaoTime;
- (void)configWithModel:(TGLSStudentList *)model;
@end
