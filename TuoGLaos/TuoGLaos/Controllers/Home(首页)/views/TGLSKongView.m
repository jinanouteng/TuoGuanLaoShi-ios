//
//  TGLSKongView.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/28.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSKongView.h"

@implementation TGLSKongView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)setup{
    UIImageView*backView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,120 , 80)];
    UILabel*nullText = [[UILabel alloc] initWithFrame:CGRectMake(0,100,self.frame.size.width, 20)];
    
    nullText.text = @"暂无数据";
    nullText.textAlignment = NSTextAlignmentCenter;
    [self addSubview:nullText];
    backView.image = [UIImage imageNamed:@"null"];
    [self addSubview:backView];
}
@end
