//
//  TGLSHomeModel.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/25.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSHomeModel.h"

@implementation TGLSHomeModel
- (instancetype)initWithUrl:(NSString *)url title:(NSString *)title
{
    if (self = [super init]) {
        _url = url;
        _title = title;
    }
    return self;
}

- (instancetype)initWithUrl:(NSString *)url title:(NSString *)title detitle:(NSString *)detitle
{
    if (self = [super init]) {
        _url = url;
        _title = title;
        _detitle = detitle;
    }
    return self;
}

@end
@implementation TGLSTeacherListModel

@end
@implementation TGLSHomeworkListModel

@end
@implementation TGLSleave_listModel

@end
@implementation TGLSStudentList

@end
