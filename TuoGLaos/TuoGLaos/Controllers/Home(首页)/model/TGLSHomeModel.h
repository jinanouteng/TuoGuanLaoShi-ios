//
//  TGLSHomeModel.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/25.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "PPBaseModle.h"

@interface TGLSHomeModel : PPBaseModle
@property (nonatomic, copy)NSString *url;

@property (nonatomic, copy)NSString *title;

@property (nonatomic, copy)NSString *detitle;

- (instancetype)initWithUrl:(NSString *)url
                      title:(NSString *)title;


- (instancetype)initWithUrl:(NSString *)url
                      title:(NSString *)title
                    detitle:(NSString *)detitle;

@end
//班主任辅导老师列表
@interface TGLSTeacherListModel : PPBaseModle
@property (nonatomic, copy)NSString*ids;                      //记录id

@property (nonatomic, copy)NSString*phone;//电话
@property (nonatomic, copy)NSString*name;//名字
@end
//班主任学生列表
@interface TGLSStudentList : PPBaseModle
@property (nonatomic, copy)NSString*ids;                      //记录id
@property (nonatomic, copy)NSString*student_code; //学号
@property (nonatomic, copy)NSString*headimg;//头像
@property (nonatomic, copy)NSString*name;//名字
@property(nonatomic,copy)NSString*create_time;
@end
//班主任作业列表
@interface TGLSHomeworkListModel : PPBaseModle
@property (nonatomic, copy)NSString*create_time;    //时间
@property (nonatomic, copy)NSString*muban_content; //内容
@property (nonatomic, copy)NSString*ids;//记录id
@property (nonatomic, copy)NSArray*pic_list;//
@end
//请假列表（班主任）
@interface TGLSleave_listModel : PPBaseModle
@property (nonatomic, copy)NSString*ids;                      //记录id
@property (nonatomic, copy)NSString*create_time; //创建时间
@property (nonatomic, copy)NSString*content;//内容，是由
@property (nonatomic, copy)NSString*start_time;                  //开始时间
@property (nonatomic, copy)NSString*end_time;                    //结束时间
@property (nonatomic, copy)NSString*day_num;                         //请假天数
@property (nonatomic, copy)NSString*status;           //1待审核 2已通过 3未通过
@property (nonatomic, copy)NSString*type;               //类型
@property (nonatomic, copy)NSString*no_content;         //不通过理由
@property (nonatomic, copy)NSString*audit_time;                                       //审批时间
@property (nonatomic, copy)NSString*name;                       //学生姓名
@property (nonatomic, copy)NSString*student_code;  //学号
@property (nonatomic, copy)NSString*parents_name;     //家长姓名
@end
