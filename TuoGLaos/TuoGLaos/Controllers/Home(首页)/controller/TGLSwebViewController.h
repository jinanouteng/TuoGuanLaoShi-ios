//
//  TGLSwebViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLSwebViewController : DDBaseViewController
@property(nonatomic,copy)NSString*url;
@property(nonatomic,copy)NSString*navTitle;
@end
