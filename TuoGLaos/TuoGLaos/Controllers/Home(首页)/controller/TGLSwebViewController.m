//
//  TGLSwebViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSwebViewController.h"

@interface TGLSwebViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation TGLSwebViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self.navTitle isEqualToString:@"yes"]) {
        self.navigationItem.title = @"辅导记录";
    }else{
        self.navigationItem.title = @"机构简介";
    }
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    [SVProgressHUD setFont:[UIFont systemFontOfSize:12]];
    [SVProgressHUD showWithStatus:@"正在加载"];
    self.webView.delegate = self;
    self.webView.scrollView.bounces = NO;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD  dismiss];
}
- (void)returnToPrevious
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
