//
//  TGLSHomeWorkFuDaoViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/28.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSHomeWorkFuDaoViewController.h"
#import "TGLSHomeModel.h"
#import "TGLSHomeworkCollectionViewCell.h"
#import "TGLShomeworkDetailViewController.h"
#import "PGDatePicker.h"
#import "TGLSKongView.h"
@interface TGLSHomeWorkFuDaoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,PGDatePickerDelegate>

@property(nonatomic,strong)UICollectionView*collectionView;
@property(nonatomic,strong)NSMutableArray*homeworkListArray;
@property(nonatomic,strong)NSMutableArray*homeworkListArraySet; //刷新
//加载第几页
@property(nonatomic,assign)NSInteger page;

@property(nonatomic,strong)TGLSKongView*backView;
@end

@implementation TGLSHomeWorkFuDaoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"作业列表";
    // Do any additional setup after loading the view from its nib.
    //初始化
    [self createUI];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //请求数据
    if (kStringIsEmpty(UserToken)) {
        return;
    }
    NSDateFormatter *formater = [[ NSDateFormatter alloc] init];
    NSDate *curDate = [NSDate date];//获取当前日期
    [formater setDateFormat:@"yyyy-MM-dd"];//这里去掉 具体时间 保留日期
    NSString*curTime = [formater stringFromDate:curDate];
    self.todayDate.text = curTime;
    self.page = 1;
    [self.homeworkListArraySet removeAllObjects];
    //请求作业列表
    [self getHomeworkData];
    
    self.navigationController.navigationBarHidden = NO;
    
}
-(void)createUI{
    self.homeworkListArray = [[NSMutableArray alloc]init];
    self.homeworkListArraySet =[[NSMutableArray alloc]init];
    //选择日期
    [self.choiseDate addTarget:self action:@selector(choiseDateClick) forControlEvents:UIControlEventTouchUpInside];
    //查看今日
    [self.checkBtn addTarget:self action:@selector(checkClick) forControlEvents:UIControlEventTouchUpInside];
    //
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]
                                          init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 114, kScreenWidth, kScreenHeight-115) collectionViewLayout:layout];
    self.collectionView.backgroundColor = UIColorHex(0xF6F3F4);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TGLSHomeworkCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGLSHomeworkCollectionViewCell"];
    [self.view addSubview:self.collectionView];
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self.homeworkListArraySet removeAllObjects];
        [self getHomeworkData];
    }];
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page++;
        [self getHomeworkData];
    }];
    
    
    self.backView= [[TGLSKongView alloc] initWithFrame:CGRectMake(kScreenWidth/2-60,kScreenHeight/2-60,120 , 120)];
     self.backView.hidden = YES;
    [self.view addSubview: self.backView];
}
-(void)checkClick{
    NSDateFormatter *formater = [[ NSDateFormatter alloc] init];
    NSDate *curDate = [NSDate date];//获取当前日期
    [formater setDateFormat:@"yyyy-MM-dd"];//这里去掉 具体时间 保留日期
    NSString * curTime = [formater stringFromDate:curDate];
    self.todayDate.text = curTime;
    self.page = 1;
    [self.homeworkListArraySet removeAllObjects];
    [self getHomeworkData];
}
#pragma mark 日期选择
-(void)choiseDateClick{
    PGDatePicker *datePicker = [[PGDatePicker alloc]init];
    datePicker.delegate = self;
    [datePicker showWithShadeBackgroud];
    datePicker.datePickerType = PGDatePickerType2;
    datePicker.isHiddenMiddleText = false;
    datePicker.titleLabel.text = @"日期选择";
    datePicker.datePickerMode = PGDatePickerModeDate;
}
#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    
    NSLog(@"dateComponents = %ld-%ld-%ld", (long)dateComponents.year,dateComponents.month,dateComponents.day);
    self.todayDate.text = [NSString stringWithFormat:@"%ld-%ld-%ld",dateComponents.year,dateComponents.month,dateComponents.day];
    self.page = 1;
    [self.homeworkListArraySet removeAllObjects];
    [self getHomeworkData];
}
//请求数据
-(void)getHomeworkData{
    [[DDNetworkManagerDate makeUrlhomework_list:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"] search_date:self.todayDate.text num:@"" page:[NSString stringWithFormat:@"%ld",(long)self.page]] post_RequestFinshSuccess:^(id responseObject) {
        NSLog(@"%@",responseObject);
        NSNumber *code = [responseObject objectForKey:@"code"];
        if([code integerValue] == 100) {
            self.homeworkListArray = (NSMutableArray *)[NSArray yy_modelArrayWithClass:[TGLSHomeworkListModel class] json:responseObject[@"list"]];
            for (int i = 0; i<self.homeworkListArray.count; i++) {
                TGLSleave_listModel*model = self.homeworkListArray[i];
                [self.homeworkListArraySet addObject:model];
            }
        }
        if ([code integerValue] == 102) {
            
            [USER_DEFAULT setObject:@"" forKey:@"token"];
            [USER_DEFAULT synchronize];
            [XHToast showCenterWithText:responseObject[@"msg"]];
            TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
            
            [self.navigationController pushViewController:login animated:YES];
        }
        if (self.homeworkListArraySet.count == 0){
            self.backView.hidden = NO;
        }else{
          self.backView.hidden = YES;
        }
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        [self.collectionView reloadData];
    } failure:^(id errorObject) {
        
    }];
}
#pragma mark - <UICollectionViewDataSource>
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.homeworkListArraySet.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *gridcell = nil;
    TGLSHomeworkCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TGLSHomeworkCollectionViewCell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 10;
    cell.layer.masksToBounds = YES;
    gridcell = cell;
    [cell configWithModel:self.homeworkListArraySet[indexPath.row]];
    return gridcell;
}

#pragma mark - item宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    TGLSHomeworkListModel*model = self.homeworkListArraySet[indexPath.row];
    
    CGRect rect = [model.muban_content boundingRectWithSize:CGSizeMake(kScreenWidth-20, kScreenHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    return CGSizeMake(kScreenWidth-20,rect.size.height+70);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    TGLShomeworkDetailViewController*dv = [[TGLShomeworkDetailViewController alloc]init];
    TGLSHomeworkListModel*model = self.homeworkListArraySet[indexPath.row];
    dv.pic_list = model.pic_list;
    dv.createTimeText = model.create_time;
    dv.context = model.muban_content;
    
    [self.navigationController pushViewController:dv animated:YES];;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 0, 10);
}
#pragma mark - head宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
