//
//  TGLSHomeWorkFuDaoViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/28.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLSHomeWorkFuDaoViewController : DDBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *todayDate;
@property (weak, nonatomic) IBOutlet UIButton *choiseDate;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;

@end
