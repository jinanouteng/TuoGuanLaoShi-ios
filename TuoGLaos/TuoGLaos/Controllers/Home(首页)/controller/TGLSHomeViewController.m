//
//  TGLSHomeViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/21.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSHomeViewController.h"
#import "TGLSwebViewController.h"
#import "TGLSMyNewViewController.h"
#import "TGLSqingJiaViewController.h"
#import "TGLSDirectorViewController.h"//班主任
#import "TGLSHomeWorkBackViewController.h"//作业反馈
#import "TGLSFuDaoQianDaoViewController.h" //辅导签到
#import "TGLSFuDaoqianTuiViewController.h" //辅导签退
#import "TGLSHomeWorkFuDaoViewController.h" //作业辅导
#import "TGLSSTeacherViewController.h" //学管师
@interface TGLSHomeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView*tableView;
@property(nonatomic,strong)NSArray*titleArray;//名称
@property(nonatomic,strong)NSArray*imageArray;//图片
@end

@implementation TGLSHomeViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.title = @"智慧家";
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-49) style:UITableViewStyleGrouped];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorHex(0xF4F2F3);
     self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
//头部视图
    UIImageView*imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenWidth/7*3)];
    imageview.image = [UIImage imageNamed:@"d070defc2906c6a40485ffe2e44178b3"];
    [self.tableView setTableHeaderView:imageview];
     self.titleArray = @[@"机构简介",@"学管师",@"教师信息",@"辅导签到",@"辅导签退",@"作业辅导",@"作业反馈",@"班主任",@"请假审批"];
    self.imageArray = @[@"机构简介",@"列表",@"名册管理",@"上课",@"上下课",@"辅导",@"意见反馈(1)",@"提交到校记录",@"饭店"];
    // Do any additional setup after loading the view from its nib.
}
#pragma mark UITableViewDelegate,UITableViewDataSource 代理实现
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString*indentifier = @"identifier";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:indentifier ];
    if (cell ==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indentifier];
          cell.selectionStyle = 0;
        int margin = 1;
        cell.backgroundColor = UIColorHex(0xF4F2F3);
        for (int i=0; i<self.titleArray.count; i++) {
            int row=i/3;//行号
            int loc=i%3;//列号
            CGFloat appviewx=margin+(margin+(kScreenWidth-2*margin)/3)*loc;
            CGFloat appviewy=margin+(margin+(kScreenWidth-2*margin)/3)*row;
            UIView*backView = [[UIView alloc]initWithFrame:CGRectMake(appviewx, appviewy,(kScreenWidth-2*margin)/3,(kScreenWidth-2*margin)/3)];
            backView.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:backView];
            UIImageView*imageView = [[UIImageView alloc] init];
            imageView.image = [UIImage imageNamed:self.imageArray[i]];
             [backView addSubview:imageView];
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(backView).with.offset((kScreenWidth-2*margin)/6-20);
                make.top.equalTo(backView).with.offset((kScreenWidth-2*margin)/6-30);
                 make.height.mas_equalTo(40);
                make.width.mas_equalTo(34);
            }];
           
            
            UILabel*label = [[UILabel alloc] initWithFrame:CGRectMake(0, (kScreenWidth-2*margin)/3-30, (kScreenWidth-2*margin)/3, 20)];
            label.text = self.titleArray[i];
            label.textAlignment = NSTextAlignmentCenter;
            [backView addSubview:label];
            
            UIButton*clickButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, (kScreenWidth-2*margin)/3, (kScreenWidth-2*margin)/3)];
            clickButton.tag = i+1;
            [clickButton addTarget:self action:@selector(enterOtherPage:) forControlEvents:UIControlEventTouchUpInside];
            [backView addSubview:clickButton];
            
        }

    }
    return cell;
}
#pragma mark 项目功能的点击事件
-(void)enterOtherPage:(UIButton*)sender{
  
 if (kStringIsEmpty(UserToken)) {
     TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];

     [self.navigationController pushViewController:login animated:YES];
    
 }else{
    
     if (sender.tag == 1) {
         TGLSwebViewController*web = [[TGLSwebViewController alloc] init];
         web.url = [NSString stringWithFormat:@"http://sysbxuqb.cnhv6.hostpod.cn/api/Htmlview/school_detail/school_id/%@/login_id/%@/token/%@",[USER_DEFAULT objectForKey:@"school_id"],[USER_DEFAULT objectForKey:@"login_id"],UserToken];
         [self.navigationController pushViewController:web animated:YES];
     }else if (sender.tag ==2){
         if ([[USER_DEFAULT objectForKey:@"teacher_type"] isEqualToString:@"3"]) {
             TGLSSTeacherViewController*tcv = [[TGLSSTeacherViewController alloc] init];
             [self.navigationController pushViewController:tcv animated:YES];
             
         }else{
               [self showCenterMessage:@"当前不是学管师,请切换账号登录"];
         }
     }else if (sender.tag == 3){
         NSLog(@"教师信息");
         TGLSMyNewViewController*new = [[TGLSMyNewViewController alloc] init];
         [self.navigationController pushViewController:new animated:YES];
     }else if (sender.tag == 4){
         NSLog(@"辅导签到");
         if ([[USER_DEFAULT objectForKey:@"teacher_type"] isEqualToString:@"1"]) {
             TGLSFuDaoQianDaoViewController*fv = [[TGLSFuDaoQianDaoViewController alloc] init];
             [self.navigationController pushViewController:fv animated:YES];
         }else{
             [self showCenterMessage:@"当前不是任课老师,请切换账号登录"];
         }
     }else if (sender.tag == 5){
         if ([[USER_DEFAULT objectForKey:@"teacher_type"] isEqualToString:@"1"]) {
             TGLSFuDaoqianTuiViewController*fv = [[TGLSFuDaoqianTuiViewController alloc] init];
             [self.navigationController pushViewController:fv animated:YES];
         }else{
             [self showCenterMessage:@"当前不是任课老师,请切换账号登录"];
         }
         NSLog(@"辅导签退");
     }else if (sender.tag == 6){
         NSLog(@"作业辅导");
         if ([[USER_DEFAULT objectForKey:@"teacher_type"] isEqualToString:@"1"]) {
             TGLSHomeWorkFuDaoViewController*fv = [[TGLSHomeWorkFuDaoViewController alloc] init];
             [self.navigationController pushViewController:fv animated:YES];
         }else{
             [self showCenterMessage:@"当前不是任课老师,请切换账号登录"];
         }
         
     }else if (sender.tag == 7){
         NSLog(@"作业反馈");
         if ([[USER_DEFAULT objectForKey:@"teacher_type"] isEqualToString:@"2"]) {
               TGLSHomeWorkBackViewController*webv = [[TGLSHomeWorkBackViewController alloc] init];
             webv.url = [NSString stringWithFormat:@"http://sysbxuqb.cnhv6.hostpod.cn/Api/Htmlview/couple_list/token/%@/login_id/%@/school_id/%@",UserToken,[USER_DEFAULT objectForKey:@"login_id"],[USER_DEFAULT objectForKey:@"school_id"]];
             [self.navigationController pushViewController:webv animated:YES];
         }else{
             [self showCenterMessage:@"当前不是班主任,请切换账号登录"];
         }
     }else if (sender.tag == 8){
         NSLog(@"班主任");
         if ([[USER_DEFAULT objectForKey:@"teacher_type"] isEqualToString:@"2"]) {
             TGLSDirectorViewController*dv = [[TGLSDirectorViewController alloc] init];
             [self.navigationController pushViewController:dv animated:YES];
         }else{
             [self showCenterMessage:@"当前不是班主任,请切换账号登录"];
         }
     }else {
         if ([[USER_DEFAULT objectForKey:@"teacher_type"] isEqualToString:@"2"]) {
             TGLSqingJiaViewController*tJia = [[TGLSqingJiaViewController alloc] init];
             [self.navigationController pushViewController:tJia animated:YES];
         }else{
             [self showCenterMessage:@"当前不是班主任,请切换账号登录"];
         }
         NSLog(@"请假审批");
     }
 }
}
//自定义组头的颜色和字体
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView*view = [[UIView alloc]init];
    view.backgroundColor = [UIColor lightTextColor];

    UILabel*label = [[UILabel alloc]initWithFrame:CGRectMake(10,10,kScreenWidth*0.3 , 20)];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = @"校园服务";
    label.font = [UIFont systemFontOfSize:20];
    [view addSubview:label];
    return view;
}

//返回cell的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (kScreenWidth-2)/3*3+4 ;
}
//返回组头的高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
