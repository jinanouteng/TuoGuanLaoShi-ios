//
//  TGLSFuDaoQianDaoViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/27.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLSFuDaoQianDaoViewController : DDBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *todayDate;
@property (weak, nonatomic) IBOutlet UIButton *choiseDate;
@property (weak, nonatomic) IBOutlet UIView *tableViewHead;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *erWeiMaBtn;

@end
