//
//  TGLShomeworkDetailViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLShomeworkDetailViewController : DDBaseViewController
@property(nonatomic,copy)NSString*createTimeText;
@property(nonatomic,copy)NSString*context;
@property(nonatomic,strong)NSArray*pic_list;
@end
