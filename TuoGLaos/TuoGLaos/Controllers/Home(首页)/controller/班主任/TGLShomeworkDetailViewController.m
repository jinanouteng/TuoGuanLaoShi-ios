//
//  TGLShomeworkDetailViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLShomeworkDetailViewController.h"

@interface TGLShomeworkDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong)UILabel*createTime;
@property(nonatomic,strong)UILabel*contentText;
@property(nonatomic,strong)UITableView*tableView;
@end

@implementation TGLShomeworkDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       self.navigationItem.title = @"作业详情";
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10,10, kScreenWidth-20, kScreenHeight-20) style:UITableViewStylePlain];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColorHex(0xF4F2F3);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
}

#pragma mark UITableViewDelegate,UITableViewDataSource 代理实现
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect rect = [self.context boundingRectWithSize:CGSizeMake(kScreenWidth-20, kScreenHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} context:nil];
    
    static NSString*indentifier = @"identifier";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:indentifier ];
    if (cell ==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indentifier];
        cell.selectionStyle = 0;
        cell.backgroundColor = UIColorHex(0xF4F2F3);
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.contentView.layer.cornerRadius = 10;
        cell.contentView.layer.masksToBounds = YES;
     
        
            self.createTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 10,kScreenWidth-40,20)];
            self.createTime.textAlignment = NSTextAlignmentRight;
            self.createTime.text = self.createTimeText;
            [cell.contentView  addSubview:self.createTime];
        
            //内容
            self.contentText = [[UILabel alloc] initWithFrame:CGRectMake(10, 40,kScreenWidth-40,rect.size.height)];
            self.contentText.numberOfLines = 0;
            self.contentText.text = self.context;
            [cell.contentView addSubview:self.contentText];
        
        
            for (int i = 0; i<self.pic_list.count; i++) {
                UIImageView*imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (210)*i+(self.contentText.origin.y+10+self.contentText.frame.size.height), kScreenWidth-40, 200)];
        
                [imageView sd_setImageWithURL:[NSURL URLWithString:self.pic_list[i]] placeholderImage:nil];
                [cell.contentView  addSubview:imageView];
            }
        
    }
    return cell;
}
//返回cell的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     CGRect rect = [self.context boundingRectWithSize:CGSizeMake(kScreenWidth-20, kScreenHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    return 200*self.pic_list.count+rect.size.height + 80;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
