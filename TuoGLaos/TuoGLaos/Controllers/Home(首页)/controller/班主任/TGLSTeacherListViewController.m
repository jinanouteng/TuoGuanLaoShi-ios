//
//  TGLSTeacherListViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSTeacherListViewController.h"
#import "TGLSHomeModel.h"
#import "TGLSTeacherListTableViewCell.h"
#import "TGLSBuQianViewController.h"
@interface TGLSTeacherListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView*tableView;
@property(nonatomic,strong)NSMutableArray*teacherListArray;//学生列表数组
@end

@implementation TGLSTeacherListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self.ways isEqualToString:@"yes"]) {
        self.navigationItem.title = @"辅导签到补签";
    }else{
        self.navigationItem.title = @"辅导签退补签";
    }
    
    //初始化
    [self createUI];
    //请求数据
    [self getTeacherListData];
}
-(void)createUI{
    self.teacherListArray = [[NSMutableArray alloc]init];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TGLSTeacherListTableViewCell" bundle:nil] forCellReuseIdentifier:@"TGLSTeacherListTableViewCell"];
    [self.view addSubview:self.tableView];
}
-(void)getTeacherListData{
    [[DDNetworkManagerDate makeUrlIndexmembefudao_teacher_list:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"]] post_RequestFinshSuccess:^(id responseObject) {
        
        NSNumber *code = [responseObject objectForKey:@"code"];
       
        if([code integerValue] == 100) {
             self.teacherListArray = (NSMutableArray *)[NSArray yy_modelArrayWithClass:[TGLSTeacherListModel class] json:responseObject[@"list"]];
        }
        [self.tableView reloadData];
    } failure:^(id errorObject) {
        NSLog(@"%@",errorObject);
    }];
}
#pragma mark UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.teacherListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TGLSTeacherListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TGLSTeacherListTableViewCell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.teacherNum.text = [NSString stringWithFormat:@"%ld.",indexPath.row+1];
    [cell configWithModel:self.teacherListArray[indexPath.row]];
    if (indexPath.row%2 == 0) {
        cell.backgroundColor =UIColorHex(0xC9E0C4);
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    cell.selectionStyle = 0;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TGLSBuQianViewController*bv = [[TGLSBuQianViewController alloc] init];
    TGLSTeacherListModel*model = self.teacherListArray[indexPath.row];
    bv.student_id = self.student_id;
    bv.teacher_id = model.ids;
    bv.type = self.type;
    [self.navigationController pushViewController:bv animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
