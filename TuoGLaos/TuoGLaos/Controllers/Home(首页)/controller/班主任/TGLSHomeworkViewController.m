//
//  TGLSHomeworkViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSHomeworkViewController.h"
#import "TGLSHomeModel.h"
#import "TGLSHomeworkCollectionViewCell.h"
#import "TGLShomeworkDetailViewController.h"
#import "TGLSKongView.h"
@interface TGLSHomeworkViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView*collectionView;
@property(nonatomic,strong)NSMutableArray*homeworkListArray;
@property(nonatomic,strong)TGLSKongView*backView;
@end

@implementation TGLSHomeworkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"作业列表";
    // Do any additional setup after loading the view from its nib.
    //初始化
    [self createUI];
    //请求作业列表
    [self getHomeworkData];
}
-(void)createUI{
    self.homeworkListArray = [[NSMutableArray alloc]init];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]
                                          init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) collectionViewLayout:layout];
    self.collectionView.backgroundColor = UIColorHex(0xF6F3F4);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TGLSHomeworkCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGLSHomeworkCollectionViewCell"];
    [self.view addSubview:self.collectionView];
    
    self.backView= [[TGLSKongView alloc] initWithFrame:CGRectMake(kScreenWidth/2-60,kScreenHeight/2-60,120 , 120)];
    self.backView.hidden = YES;
    [self.view addSubview: self.backView];
}
-(void)getHomeworkData{
    [[DDNetworkManagerDate makeUrlIndexmember_student_homework_list:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"] student_id:self.student_id] post_RequestFinshSuccess:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        NSNumber *code = [responseObject objectForKey:@"code"];
        if([code integerValue] == 100) {
            self.homeworkListArray = (NSMutableArray *)[NSArray yy_modelArrayWithClass:[TGLSHomeworkListModel class] json:responseObject[@"list"]];
          
        }
        if (self.homeworkListArray.count == 0){
            self.backView.hidden = NO;
        }else{
            self.backView.hidden = YES;
        }
        [self.collectionView reloadData];
    } failure:^(id errorObject) {
        
    }];
}
#pragma mark - <UICollectionViewDataSource>
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.homeworkListArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *gridcell = nil;
    TGLSHomeworkCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TGLSHomeworkCollectionViewCell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 6;
    cell.layer.masksToBounds = YES;
    gridcell = cell;
    [cell configWithModel:self.homeworkListArray[indexPath.row]];
    return gridcell;
}
//这里我为了直观的看出每组的CGSize设置用if 后续我会用简洁的三元表示
#pragma mark - item宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    TGLSHomeworkListModel*model = self.homeworkListArray[indexPath.row];
   
    CGRect rect = [model.muban_content boundingRectWithSize:CGSizeMake(kScreenWidth-20, kScreenHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
        return CGSizeMake(kScreenWidth-20,rect.size.height+70);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    TGLShomeworkDetailViewController*dv = [[TGLShomeworkDetailViewController alloc]init];
    TGLSHomeworkListModel*model = self.homeworkListArray[indexPath.row];
    dv.pic_list = model.pic_list;
    dv.createTimeText = model.create_time;
    dv.context = model.muban_content;
   
    [self.navigationController pushViewController:dv animated:YES];;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 0, 10);
}
#pragma mark - head宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
