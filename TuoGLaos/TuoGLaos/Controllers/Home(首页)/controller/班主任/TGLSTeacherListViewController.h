//
//  TGLSTeacherListViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLSTeacherListViewController : DDBaseViewController
@property(nonatomic,copy)NSString*ways;
@property(nonatomic,copy)NSString*student_id;
@property(nonatomic,copy)NSString*type;
@end
