//
//  TGLSBuQianViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLSBuQianViewController : DDBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *todayDate;
@property (weak, nonatomic) IBOutlet UIButton *dateChoise;
@property (weak, nonatomic) IBOutlet UITextView *buQianBeizhu;
@property (weak, nonatomic) IBOutlet UILabel *textone;
@property (weak, nonatomic) IBOutlet UITextView *homeWorkDetail;
@property (weak, nonatomic) IBOutlet UILabel *textTwo;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property(nonatomic,copy)NSString*student_id;
@property(nonatomic,copy)NSString*teacher_id;
@property(nonatomic,copy)NSString*type;
@end
