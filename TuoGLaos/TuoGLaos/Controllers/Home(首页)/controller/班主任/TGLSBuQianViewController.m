//
//  TGLSBuQianViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/26.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSBuQianViewController.h"
#import "PGDatePicker.h"
@interface TGLSBuQianViewController ()<PGDatePickerDelegate,UITextViewDelegate>

@end

@implementation TGLSBuQianViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
      NSDateFormatter *formater = [[ NSDateFormatter alloc] init];
    NSDate *curDate = [NSDate date];//获取当前日期
    [formater setDateFormat:@"yyyy-MM-dd"];//这里去掉 具体时间 保留日期
    NSString * curTime = [formater stringFromDate:curDate];
    self.todayDate.text = curTime;
    [self.dateChoise addTarget:self action:@selector(choiseDateClick) forControlEvents:UIControlEventTouchUpInside];
    [self.sureBtn addTarget:self action:@selector(sureClick) forControlEvents:UIControlEventTouchUpInside];
    self.sureBtn.layer.cornerRadius = 20;
    
    self.buQianBeizhu.delegate= self;
    self.buQianBeizhu.tag = 100;
    self.homeWorkDetail.tag = 101;
    self.homeWorkDetail.delegate = self;
      self.navigationItem.title = @"辅导补签";

}
-(void)textViewDidChange:(UITextView *)textView{
    if (textView.tag == 100) {
        if (self.buQianBeizhu.text.length == 0) {
            self.textone.text = @"请填写补签备注...";
        }else{
            self.textone.text = @"";
        }
    }else{
        if (self.buQianBeizhu.text.length == 0) {
            self.textone.text = @"请填写补签作业详情...";
        }else{
            self.textone.text = @"";
        }
    
    }
}
#pragma mark 日期选择
-(void)choiseDateClick{
        PGDatePicker *datePicker = [[PGDatePicker alloc]init];
        datePicker.delegate = self;
        [datePicker showWithShadeBackgroud];
        datePicker.datePickerType = PGDatePickerType2;
        datePicker.isHiddenMiddleText = false;
        datePicker.titleLabel.text = @"日期选择";
        datePicker.datePickerMode = PGDatePickerModeDate;
}

#pragma mark 确认补签
-(void)sureClick{
    if([self.buQianBeizhu.text isEqualToString:@""]){
        [self showCenterMessage:@"补签备注不能为空"];
        return;
    }
    [[DDNetworkManagerDate makeUrlIndexmembefudao_make_insert:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"] student_id:self.student_id make_date:self.todayDate.text remark:self.buQianBeizhu.text type:self.type teacher_id:self.teacher_id content:self.homeWorkDetail.text] post_RequestFinshSuccess:^(id responseObject) {
        NSLog(@"%@",responseObject);
        [self.navigationController popViewControllerAnimated:YES];
        [self showCenterMessage:responseObject[@"msg"]];
    } failure:^(id errorObject) {
        
    }];
}
#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    
    NSLog(@"dateComponents = %ld-%ld-%ld", (long)dateComponents.year,(long)dateComponents.month,dateComponents.day);
    self.todayDate.text = [NSString stringWithFormat:@"%ld-%ld-%ld",(long)dateComponents.year,dateComponents.month,dateComponents.day];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
