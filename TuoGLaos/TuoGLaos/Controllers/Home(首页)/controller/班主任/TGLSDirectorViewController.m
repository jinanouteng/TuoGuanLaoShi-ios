//
//  TGLSDirectorViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/25.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSDirectorViewController.h"
#import "TGLSTeacherTableViewCell.h"
#import "TGLSHomeworkViewController.h" //班主任作业
#import "TGLSwebViewController.h"
#import "TGLSTeacherListViewController.h"//补签
#import "TGLSKongView.h"
@interface TGLSDirectorViewController ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>
@property(nonatomic,strong)UITableView*tableView;
@property(nonatomic,strong)NSMutableArray*studentListArray;//学生列表数组
@property(nonatomic,strong)TGLSKongView*backView;
@end

@implementation TGLSDirectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //初始化
    [self createUI];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //请求数据
    if (kStringIsEmpty(UserToken)) {
        return;
    }
    //请求数据
    [self getStudentList];
}
-(void)createUI{
    self.navigationItem.title = @"学生列表";
    self.studentListArray = [[NSMutableArray alloc]init];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
 self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"TGLSTeacherTableViewCell" bundle:nil] forCellReuseIdentifier:@"TGLSTeacherTableViewCell"];
    [self.view addSubview:self.tableView];
    
    self.backView= [[TGLSKongView alloc] initWithFrame:CGRectMake(kScreenWidth/2-60,kScreenHeight/2-60,120 , 120)];
    self.backView.hidden = YES;
    [self.view addSubview: self.backView];
}
-(void)getStudentList{
    [[DDNetworkManagerDate makeUrlIndexmember_all_ban_student_list:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"]] post_RequestFinshSuccess:^(id responseObject) {
        NSLog(@"%@",responseObject);
        NSNumber *code = [responseObject objectForKey:@"code"];
        if([code integerValue] == 100) {
        self.studentListArray = (NSMutableArray *)[NSArray yy_modelArrayWithClass:[TGLSStudentList class] json:responseObject[@"list"]];
        }
        if ([code integerValue] == 102) {
            
            [USER_DEFAULT setObject:@"" forKey:@"token"];
            [USER_DEFAULT synchronize];
            [XHToast showCenterWithText:responseObject[@"msg"]];
            TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
            
            [self.navigationController pushViewController:login animated:YES];
        }
        if (self.studentListArray.count == 0){
            self.backView.hidden = NO;
        }else{
            self.backView.hidden = YES;
        }
        [self.tableView reloadData];
       } failure:^(id errorObject) {
           NSLog(@"%@",errorObject);
      }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.studentListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TGLSTeacherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TGLSTeacherTableViewCell" forIndexPath:indexPath];
      
//    cell.studentNumber.text = [NSString stringWithFormat:@"%ld.",(long)(indexPath.row+1)];
    if (indexPath.row%2 == 1) {
        cell.backgroundColor =UIColorHex(0xC9E0C4);
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    [cell configWithModel:self.studentListArray[indexPath.row]];
    cell.selectionStyle = 0;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIActionSheet *us = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"作业",@"辅导记录",@"辅导补签到",@"辅导补签退", nil];
    us.tag = indexPath.row+1;
    [us showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    TGLSStudentList*model = self.studentListArray[actionSheet.tag-1];
    NSLog(@"%@",model.ids);
    if (buttonIndex == 0) {
        NSLog(@"作业");
        TGLSHomeworkViewController*hv = [[TGLSHomeworkViewController alloc] init];
        hv.student_id = model.ids;
        [self.navigationController pushViewController:hv animated:YES];
    }else if (buttonIndex == 1){
         NSLog(@"辅导记录");
        TGLSwebViewController*web = [[TGLSwebViewController alloc] init];
        web.url = [NSString stringWithFormat:@"http://sysbxuqb.cnhv6.hostpod.cn/index.php/Api/Htmlview/fudao_list/school_id/%@/login_id/%@/token/%@/student_id/%@/search_date/%@/num/%@/page/%@",[USER_DEFAULT objectForKey:@"school_id"],[USER_DEFAULT objectForKey:@"login_id"],UserToken,model.ids,@"",@"",@""];
        web.navTitle = @"yes";
        [self.navigationController pushViewController:web animated:YES];
    }else if (buttonIndex == 2){
        NSLog(@"辅导补签到");
        TGLSTeacherListViewController*tv= [[TGLSTeacherListViewController alloc] init];
        tv.ways = @"yes";
        tv.student_id = model.ids;
        tv.type = @"1";
        [self.navigationController pushViewController:tv animated:YES];
    }else{
        NSLog(@"辅导补签退");
        TGLSTeacherListViewController*tv= [[TGLSTeacherListViewController alloc] init];
        tv.student_id = model.ids;
        tv.type = @"2";
        [self.navigationController pushViewController:tv animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
