//
//  TGLSqingJiaDetailViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/25.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSqingJiaDetailViewController.h"
#import "TGLSHomeModel.h"
@interface TGLSqingJiaDetailViewController ()<UITextViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *studentName;
@property (weak, nonatomic) IBOutlet UILabel *parentsName;
@property (weak, nonatomic) IBOutlet UILabel *thingStatus;
@property (weak, nonatomic) IBOutlet UILabel *thingDaynum;
@property (weak, nonatomic) IBOutlet UILabel *thingType;
@property (weak, nonatomic) IBOutlet UILabel *createTime;
@property (weak, nonatomic) IBOutlet UILabel *morenText;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *thingTime;
@property (weak, nonatomic) IBOutlet UILabel *thingReason;

@property (weak, nonatomic) IBOutlet UIButton *checkBtn;

@end

@implementation TGLSqingJiaDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"请假详情";
    //请求请假详情
    [self downDetaiData];
    self.textView.delegate = self;
    self.checkBtn.layer.cornerRadius = 5;
    self.checkBtn.backgroundColor = UIColorHex(0x3968DD);
    if ([self.status isEqualToString:@"2"]){
        self.textView.editable = NO;
        self.checkBtn.hidden = YES;
    }else{
        self.textView.editable = YES;
        self.checkBtn.hidden = NO;
    }
    [self.checkBtn addTarget:self action:@selector(check) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark 审核按钮点击事件
-(void)check{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否同意请假" message:@"" delegate:self cancelButtonTitle:@"不同意" otherButtonTitles:@"同意", nil];
                                      [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self check:@"3"];
            break;
            
        case 1:
            [self check:@"2"];
            break;
            
        default:
            break;
    }
}
#pragma mark 审批
-(void)check:(NSString*)context{
    [[DDNetworkManagerDate makeUrlIndexmember_leave_reply:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"] leave_id:self.leave_id up_status:context no_content:self.textView.text] post_RequestFinshSuccess:^(id responseObject) {
      
        [self showTopMessage:responseObject[@"msg"]];
    } failure:^(id errorObject) {
        
    }];
}
-(void)textViewDidChange:(UITextView *)textView{
    if (self.textView.text.length == 0) {
        self.morenText.text = @"请输入内容";
    }else{
        self.morenText.text = @"";
    }
}
-(void)downDetaiData{
    [[DDNetworkManagerDate makeUrlIndexmember_leave_detail:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"] leave_id:self.leave_id] post_RequestFinshSuccess:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        NSNumber *code = [responseObject objectForKey:@"code"];
        if([code integerValue] == 100) {
            self.studentName.text = [NSString stringWithFormat:@"%@(%@)",responseObject[@"name"],responseObject[@"student_code"]];
            self.createTime.text = responseObject[@"create_time"];
            if (kStringIsEmpty(responseObject[@"parents_name"])) {
                     self.parentsName.text = @"";
            }else{
                 self.parentsName.text = responseObject[@"parents_name"];
            }
           
            if ([self.status isEqualToString:@"1"]){
                self.thingStatus.text = @"未批";
            }else if ([self.status isEqualToString:@"2"]){
                self.thingStatus.text = @"已批";
            }else{
                self.thingStatus.text = @"拒绝";
            }
            self.thingType.text =  responseObject[@"type"];
            self.thingDaynum.text = [NSString stringWithFormat:@"%@天", responseObject[@"day_num"]];
            self.thingTime.text = [NSString stringWithFormat:@"%@--%@", responseObject[@"start_time"], responseObject[@"end_time"]];
            self.thingReason.text = responseObject[@"content"];
        }
      
    }failure:^(id errorObject) {
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
