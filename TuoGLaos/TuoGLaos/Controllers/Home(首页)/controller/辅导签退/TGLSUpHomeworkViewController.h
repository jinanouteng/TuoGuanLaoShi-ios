//
//  TGLSUpHomeworkViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/28.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLSUpHomeworkViewController : DDBaseViewController
@property (weak, nonatomic) IBOutlet UITextView *homeworkText;
@property (weak, nonatomic) IBOutlet UILabel *morenText;
@property(nonatomic,copy)NSString*student_id;
@end
