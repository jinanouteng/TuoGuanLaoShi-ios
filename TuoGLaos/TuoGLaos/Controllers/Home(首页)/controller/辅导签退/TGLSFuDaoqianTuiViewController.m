//
//  TGLSFuDaoqianTuiViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/28.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSFuDaoqianTuiViewController.h"
#import "TGLSTeacherTableViewCell.h"
#import "TGLSUpHomeworkViewController.h"
@interface TGLSFuDaoqianTuiViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView*tableView;
@property(nonatomic,strong)NSMutableArray*studentListArray;//学生列表数组
@property(nonatomic,strong)NSMutableArray*studentListArraySet; //数组应用数组
//加载第几页
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,strong)TGLSKongView*backView;
@end

@implementation TGLSFuDaoqianTuiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //初始化
    [self createUI];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //请求数据
    if (kStringIsEmpty(UserToken)) {
        return;
    }
    self.page = 1;
    //请求数据
    [self.studentListArraySet removeAllObjects];
    //请求数据
    [self getStudentList];
}
-(void)createUI{
    self.navigationItem.title = @"学生列表";
    self.studentListArray = [[NSMutableArray alloc]init];
    self.studentListArraySet = [[NSMutableArray alloc]init];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
     self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"TGLSTeacherTableViewCell" bundle:nil] forCellReuseIdentifier:@"TGLSTeacherTableViewCell"];
    [self.view addSubview:self.tableView];
    
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self.studentListArraySet removeAllObjects];
        [self getStudentList];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page++;
        [self getStudentList];
    }];
    
    
    self.backView= [[TGLSKongView alloc] initWithFrame:CGRectMake(kScreenWidth/2-60,kScreenHeight/2-60,120 , 120)];
    self.backView.hidden = YES;
    [self.view addSubview: self.backView];
}
-(void)getStudentList{
    [[DDNetworkManagerDate makeUrlhomework_sing_doing:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"]  login_id:[USER_DEFAULT objectForKey:@"login_id"] num:@"" page:[NSString stringWithFormat:@"%ld",(long)self.page]] post_RequestFinshSuccess:^(id responseObject) {
        
        NSNumber *code = [responseObject objectForKey:@"code"];
        if([code integerValue] == 100) {
            self.studentListArray = (NSMutableArray *)[NSArray yy_modelArrayWithClass:[TGLSStudentList class] json:responseObject[@"list"]];
            for (int i = 0; i<self.studentListArray.count; i++) {
                TGLSleave_listModel*model = self.studentListArray[i];
                [self.studentListArraySet addObject:model];
            }
        }
        if ([code integerValue] == 102) {
            
            [USER_DEFAULT setObject:@"" forKey:@"token"];
            [USER_DEFAULT synchronize];
            [XHToast showCenterWithText:responseObject[@"msg"]];
            TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
            
            [self.navigationController pushViewController:login animated:YES];
        }
        if (self.studentListArraySet.count == 0) {
            self.backView.hidden = NO;
         
            self.tableView.scrollEnabled = NO;
        }else{
            self.tableView.scrollEnabled = YES;
          
            self.backView.hidden = YES;
        }
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failure:^(id errorObject) {
        NSLog(@"%@",errorObject);
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.studentListArraySet.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TGLSTeacherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TGLSTeacherTableViewCell" forIndexPath:indexPath];
    
//    cell.studentNumber.text = [NSString stringWithFormat:@"%ld.",indexPath.row+1];
    if (indexPath.row%2 == 1) {
        cell.backgroundColor =UIColorHex(0xC9E0C4);
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    [cell configWithModel:self.studentListArraySet[indexPath.row]];
    cell.selectionStyle = 0;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TGLSUpHomeworkViewController*sv = [[TGLSUpHomeworkViewController alloc] init];
    TGLSStudentList*model = self.studentListArraySet[indexPath.row];
    sv.student_id = model.ids;
    [self.navigationController pushViewController:sv animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
