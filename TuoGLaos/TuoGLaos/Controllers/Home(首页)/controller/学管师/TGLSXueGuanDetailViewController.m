//
//  TGLSXueGuanDetailViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/29.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSXueGuanDetailViewController.h"
#import <WebKit/WebKit.h>
@interface TGLSXueGuanDetailViewController ()<WKUIDelegate,WKNavigationDelegate>
@property(nonatomic,strong)WKWebView*webView;
@end

@implementation TGLSXueGuanDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight-64)];
    self.view.backgroundColor = [UIColor whiteColor];
    self.webView.backgroundColor = [UIColor whiteColor];
    if ([self.navTitle isEqualToString:@"yes"]) {
        self.navigationItem.title = @"学管师签到详情";
    }else{
        self.navigationItem.title = @"学管师签退详情";
    }
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    [SVProgressHUD setFont:[UIFont systemFontOfSize:12]];
    [SVProgressHUD showWithStatus:@"正在加载"];
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    self.webView.scrollView.bounces = NO;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD  dismiss];
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    
    NSLog(@"页面加载完成");
    [SVProgressHUD dismiss];
    [self.view addSubview:_webView];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    return YES;
}
@end
