//
//  TGLSSTeacherViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSSTeacherViewController.h"
#import "TGLSTeacherTableViewCell.h"
#import "TGLSXueGuanDetailViewController.h"
@interface TGLSSTeacherViewController ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>
@property(nonatomic,strong)UITableView*tableView;
@property(nonatomic,strong)NSMutableArray*studentListArray;//学生列表数组
@property(nonatomic,strong)TGLSKongView*backView;
@property(nonatomic,copy)NSString*search_date;

@end

@implementation TGLSSTeacherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //初始化
    [self createUI];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //请求数据
    if (kStringIsEmpty(UserToken)) {
        return;
    }
    //请求数据
    [self getStudentList];
}
-(void)createUI{
  
    NSDateFormatter *formater = [[ NSDateFormatter alloc] init];
    NSDate *curDate = [NSDate date];//获取当前日期
    [formater setDateFormat:@"yyyy-MM"];
    self.search_date = [formater stringFromDate:curDate];
    
    self.navigationItem.title = @"学管师学生列表";
    self.studentListArray = [[NSMutableArray alloc]init];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TGLSTeacherTableViewCell" bundle:nil] forCellReuseIdentifier:@"TGLSTeacherTableViewCell"];
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.backView= [[TGLSKongView alloc] initWithFrame:CGRectMake(kScreenWidth/2-60,kScreenHeight/2-60,120 , 120)];
    self.backView.hidden = YES;
    [self.view addSubview: self.backView];
}
-(void)getStudentList{
    [[DDNetworkManagerDate makeUrlall_student_list:UserToken school_id:[USER_DEFAULT objectForKey:@"school_id"]  login_id:[USER_DEFAULT objectForKey:@"login_id"]] post_RequestFinshSuccess:^(id responseObject) {
        
        NSNumber *code = [responseObject objectForKey:@"code"];
        if([code integerValue] == 100) {
            self.studentListArray = (NSMutableArray *)[NSArray yy_modelArrayWithClass:[TGLSStudentList class] json:responseObject[@"list"]];
        }
        if ([code integerValue] == 102) {
            
            [USER_DEFAULT setObject:@"" forKey:@"token"];
            [USER_DEFAULT synchronize];
            [XHToast showCenterWithText:responseObject[@"msg"]];
            TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
            
            [self.navigationController pushViewController:login animated:YES];
        }
        if (self.studentListArray.count == 0) {
            self.backView.hidden = NO;
            
            self.tableView.scrollEnabled = NO;
        }else{
            self.tableView.scrollEnabled = YES;
            
            self.backView.hidden = YES;
        }
        [self.tableView reloadData];
    } failure:^(id errorObject) {
        NSLog(@"%@",errorObject);
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.studentListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TGLSTeacherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TGLSTeacherTableViewCell" forIndexPath:indexPath];

//    cell.studentNumber.text = [NSString stringWithFormat:@"%ld.",(long)(indexPath.row+1)];
    if (indexPath.row%2 == 1) {
        cell.backgroundColor =UIColorHex(0xC9E0C4);
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    [cell configWithModel:self.studentListArray[indexPath.row]];
    cell.selectionStyle = 0;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIActionSheet *us = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"签到详情",@"签退详情", nil];
    us.tag = indexPath.row+1;
    [us showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    TGLSStudentList*model = self.studentListArray[actionSheet.tag-1];
    NSLog(@"%@",model.ids);
    if (buttonIndex == 0) {
        NSLog(@"签到详情");
        TGLSXueGuanDetailViewController*xgv = [[TGLSXueGuanDetailViewController alloc] init];
        xgv.url = [NSString stringWithFormat:@"http://sysbxuqb.cnhv6.hostpod.cn/index.php/Api/Htmlview/sing_list/search_date/%@/school_id/%@/login_id/%@/token/%@/student_id/%@.html",self.search_date,[USER_DEFAULT objectForKey:@"school_id"],[USER_DEFAULT objectForKey:@"login_id"],UserToken,model.ids];
        xgv.navTitle = @"yes";
        [self.navigationController pushViewController:xgv animated:YES];
     
    }else if (buttonIndex == 1){
        NSLog(@"签退详情");
        TGLSXueGuanDetailViewController*xgv = [[TGLSXueGuanDetailViewController alloc] init];
        xgv.url = [NSString stringWithFormat:@"http://sysbxuqb.cnhv6.hostpod.cn/index.php/Api/Htmlview/sing_tui_list/search_date/%@/school_id/%@/login_id/%@/token/%@/student_id/%@.html",self.search_date,[USER_DEFAULT objectForKey:@"school_id"],[USER_DEFAULT objectForKey:@"login_id"],UserToken,model.ids];
        [self.navigationController pushViewController:xgv animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
