//
//  TGLSMynewTableViewCell.h
//  TuoGLaos
//
//  Created by 欧腾 on 2018/1/5.
//  Copyright © 2018年 outeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGLSMynewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *teacherImage;
@property (weak, nonatomic) IBOutlet UILabel *teacherName;
@property (weak, nonatomic) IBOutlet UILabel *teacherSex;
@property (weak, nonatomic) IBOutlet UILabel *teacherAcademic;
@property (weak, nonatomic) IBOutlet UILabel *teacherPhone;
@property (weak, nonatomic) IBOutlet UILabel *teacherDate;
@property (weak, nonatomic) IBOutlet UILabel *teacherCertificate;
@property (weak, nonatomic) IBOutlet UILabel *teacherSchool;
@property (weak, nonatomic) IBOutlet UILabel *teacherAddress;
@property (weak, nonatomic) IBOutlet UILabel *teacherAge;

@end
