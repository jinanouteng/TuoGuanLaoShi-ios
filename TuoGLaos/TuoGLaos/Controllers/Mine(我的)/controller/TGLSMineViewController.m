//
//  TGLSMineViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/21.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSMineViewController.h"
#import "TGLSMyNewViewController.h"
@interface TGLSMineViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headView;//头视图
@property (weak, nonatomic) IBOutlet UIImageView *teacherPhoto;//头像
@property (weak, nonatomic) IBOutlet UILabel *teacherTitle;//名称
@property (weak, nonatomic) IBOutlet UILabel *teacherNum;//号码
@property (weak, nonatomic) IBOutlet UILabel *showText;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property(nonatomic,strong)NSArray*textArray;
@end

@implementation TGLSMineViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    if (kStringIsEmpty(UserToken)) {
         self.teacherPhoto.image = [UIImage imageNamed:@"老师"];
        self.teacherTitle.text = @"";
        self.teacherNum.text= @"";
        self.showText.text = @"尚未登录,点击登录";
        [self.loginBtn addTarget:self action:@selector(loginView) forControlEvents:UIControlEventTouchUpInside];
        self.loginBtn.hidden = NO;
    }else{
        self.loginBtn.hidden = YES;
        [self.teacherPhoto sd_setImageWithURL:[NSURL URLWithString:[USER_DEFAULT objectForKey:@"headimg"]] placeholderImage:[UIImage imageNamed:@"老师"]];
        self.teacherTitle.text = [USER_DEFAULT objectForKey:@"teacher_name"];
        self.teacherNum.text = [USER_DEFAULT objectForKey:@"phones"];
        self.showText.text = @"";
    }
}
-(void)loginView{
    TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
    [self.navigationController pushViewController:login animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //定义头视图高度
    self.headView.frame = CGRectMake(0, 0, kScreenWidth, 110);
    self.headView.backgroundColor = UIColorHex(0x3968DD);
    //头像
    self.teacherPhoto.backgroundColor = [UIColor whiteColor];
    self.teacherPhoto.layer.cornerRadius = 30;
    self.teacherPhoto.layer.masksToBounds = YES;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.textArray = @[@"我的信息",@"版本检测",@"安全退出"];
    self.tableView.tableFooterView = [UIView new];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark UITableViewDelegate,UITableViewDataSource 代理实现
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString*indentifier = @"identifier";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:indentifier ];
    if (cell ==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indentifier];
        cell.selectionStyle = 0;
        UILabel*label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 80, 30)];
        label.text = self.textArray[indexPath.row];
        label.font = [UIFont systemFontOfSize:19];
        [cell.contentView addSubview:label];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row == 1) {
            NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

            // app版本
            NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
            UILabel*version = [[UILabel alloc] initWithFrame:CGRectMake(kScreenWidth-80, 10, 40, 30)];
            version.text = app_Version;
            [cell.contentView addSubview:version];
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   if (kStringIsEmpty(UserToken)) {
       TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
       [self.navigationController pushViewController:login animated:YES];
   }else{
       if (indexPath.row == 0) {
           TGLSMyNewViewController*new = [[TGLSMyNewViewController alloc] init];
           [self.navigationController pushViewController:new animated:YES];
       }else if (indexPath.row ==1){
           //版本检测
//           [[DDNetworkManagerDate app_update:@"2" app_code:@"1.0"] post_RequestFinshSuccess:^(id responseObject) {
//               NSLog(@"%@",responseObject);
//           } failure:^(id errorObject) {
//
//           }];

       }else{
           UIAlertView*alertView = [[UIAlertView alloc] initWithTitle:@"账号管理" message:@"你确定要退出此账号吗" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定",@"取消", nil];
           [alertView show];

       }
   }
  
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
        
        [self.navigationController pushViewController:login animated:YES];
    }else{
        NSLog(@"取消");
    }
}
//返回cell的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
