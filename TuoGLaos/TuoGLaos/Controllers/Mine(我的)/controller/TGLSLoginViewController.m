//
//  TGLSLoginViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/21.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSLoginViewController.h"

@interface TGLSLoginViewController ()
@property (weak, nonatomic) IBOutlet UIView *backView;
- (IBAction)backButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITextField *mimaTextField;
@property (weak, nonatomic) IBOutlet UIView *backView2;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;//登录按钮


@end

@implementation TGLSLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //初始化UI
    [self createUI];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
}
-(void)createUI{
    self.backView.layer.cornerRadius = 20;
    self.view.backgroundColor = UIColorHex(0x2772F1);
    self.backView.backgroundColor = UIColorHex(0x2772F1);
    self.backView.layer.borderWidth = 1;
    self.backView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.backView2.layer.cornerRadius = 20;
    self.backView2.backgroundColor = UIColorHex(0x2772F1);
    self.backView2.layer.borderWidth = 1;
    self.backView2.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    [self.textField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.mimaTextField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.textField.tintColor = [UIColor blackColor];
    self.mimaTextField.tintColor = [UIColor blackColor];
    [USER_DEFAULT removeObjectForKey:@"token"];
    self.textField.text = [USER_DEFAULT objectForKey:@"phone"];
    self.mimaTextField.text = [USER_DEFAULT objectForKey:@"pwd"];
    self.loginButton.layer.cornerRadius = 20;
    [self.loginButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark 登录
-(void)login:(UIButton*)sender{
    if (self.textField.text.length<=0) {
        [self showCenterMessage:@"请输入用户名"];
        return;
    }
    if (self.mimaTextField.text.length <=0) {
        [self showCenterMessage:@"请输入密码"];
        return;
    }
    //登录
    
    [[DDNetworkManagerDate makeUrlIndexmember_login:self.textField.text member_pwd:self.mimaTextField.text] post_RequestFinshSuccess:^(id responseObject) {
        NSLog(@"%@",responseObject);
        NSNumber *code = [responseObject objectForKey:@"code"];
        
        if ([code integerValue] == 100) {
            [self showCenterMessage:@"登录成功"];
            
            NSString *token = [responseObject objectForKey:@"token"];//标识
            NSLog(@"%@",token);
            NSString *login_id = [NSString stringWithFormat:@"%@", [responseObject objectForKey:@"login_id"]];
            NSString*teacher_name = [responseObject objectForKey:@"teacher_name"];
            NSString*phone = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"phone"]];
            NSString *headimg = [responseObject objectForKey:@"headimg"];
            NSString*school_id = [NSString stringWithFormat:@"%@", [responseObject objectForKey:@"school_id"]];
             NSString*teacher_type = [NSString stringWithFormat:@"%@", [responseObject objectForKey:@"teacher_type"]];
            NSUserDefaults *DEF = [NSUserDefaults standardUserDefaults];
            [DEF setObject:token forKey:@"token"];
            [DEF setObject:teacher_name forKey:@"teacher_name"];
             [DEF setObject:phone forKey:@"phones"];
             [DEF setObject:login_id forKey:@"login_id"];
            [DEF setObject:headimg forKey:@"headimg"];
            [DEF setObject:school_id forKey:@"school_id"];
            [DEF setObject:teacher_type forKey:@"teacher_type"];
            
            [DEF setObject:self.textField.text forKey:@"phone"];
            [DEF setObject:self.mimaTextField.text forKey:@"pwd"];
            
            [DEF synchronize];
            
                [self.navigationController popViewControllerAnimated:YES];

        }else{
            [self showCenterMessage:responseObject[@"msg"]];
        }
    } failure:^(id errorObject) {
        
    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)backButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}
@end
