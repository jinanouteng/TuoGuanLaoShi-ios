//
//  TGLSMyNewViewController.h
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "DDBaseViewController.h"

@interface TGLSMyNewViewController : DDBaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *teacherImage;
@property (weak, nonatomic) IBOutlet UILabel *teacherName;
@property (weak, nonatomic) IBOutlet UILabel *teacherSex;
@property (weak, nonatomic) IBOutlet UILabel *teacherAcademic;
@property (weak, nonatomic) IBOutlet UILabel *teacherPhone;
@property (weak, nonatomic) IBOutlet UILabel *teacherDate;
@property (weak, nonatomic) IBOutlet UILabel *teacherCertificate;
@property (weak, nonatomic) IBOutlet UILabel *teacherSchool;
@property (weak, nonatomic) IBOutlet UILabel *teacherAddress;
@property (weak, nonatomic) IBOutlet UILabel *teacherAge;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *teacherView;

@end
