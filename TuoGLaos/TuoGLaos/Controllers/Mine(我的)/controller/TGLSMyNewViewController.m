//
//  TGLSMyNewViewController.m
//  TuoGLaos
//
//  Created by 欧腾 on 2017/12/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "TGLSMyNewViewController.h"
#import "TGLSMynewTableViewCell.h"
@interface TGLSMyNewViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSDictionary*dict;
@end

@implementation TGLSMyNewViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
    //请求数据
    if (kStringIsEmpty(UserToken)) {
        return;
    }
    //请求数据
    [self getTeacherNews];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"教师详情";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = UIColorHex(0xF4F2F3);
     self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.layer.cornerRadius = 10;
    self.tableView.layer.masksToBounds = YES;
    [self.tableView registerNib:[UINib nibWithNibName:@"TGLSMynewTableViewCell" bundle:nil] forCellReuseIdentifier:@"TGLSMynewTableViewCell"];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     TGLSMynewTableViewCell   *cell = [tableView dequeueReusableCellWithIdentifier:@"TGLSMynewTableViewCell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 10;
    cell.layer.masksToBounds = YES;
//    cell.contentView.layer.cornerRadius = 10;
//    cell.contentView.layer.masksToBounds = YES;
    cell.teacherImage.layer.cornerRadius = 40;
    cell.teacherImage.layer.masksToBounds = YES;
    cell.teacherAddress.numberOfLines = 0;
    cell.teacherAddress.adjustsFontSizeToFitWidth = YES;
        
    [cell.teacherImage sd_setImageWithURL:[NSURL URLWithString:[self.dict objectForKey:@"headimg"]] placeholderImage:[UIImage imageNamed:@"老师"]];
    cell.teacherName.text = [self.dict objectForKey:@"name"];
    cell.teacherSex.text = [self.dict objectForKey:@"sex"];
    cell.teacherAcademic.text = [self.dict objectForKey:@"education"];
    cell.teacherPhone.text = [self.dict objectForKey:@"phone"];
    cell.teacherDate.text = [self.dict objectForKey:@"birthday"];
    cell.teacherCertificate.text = [self.dict objectForKey:@"have_cert"];
    cell.teacherAge.text = [self.dict objectForKey:@"work_time"];
    cell.teacherSchool.text = [self.dict objectForKey:@"campus_name"];
    cell.teacherAddress.text = [self.dict objectForKey:@"address"];
    cell.selectionStyle = 0;
    return cell;
}
-(void)getTeacherNews{
    [[DDNetworkManagerDate makeUrlIndexmember_teacherNews:UserToken school_id: [USER_DEFAULT objectForKey:@"school_id"] login_id:[USER_DEFAULT objectForKey:@"login_id"]] post_RequestFinshSuccess:^(id responseObject) {
        NSNumber *code = [responseObject objectForKey:@"code"];
        if ([code integerValue] == 102) {
            
            [USER_DEFAULT setObject:@"" forKey:@"token"];
            [USER_DEFAULT synchronize];
            [XHToast showCenterWithText:responseObject[@"msg"]];
            TGLSLoginViewController*login = [[TGLSLoginViewController alloc]init];
            
            [self.navigationController pushViewController:login animated:YES];
            return ;
        }
        self.dict = responseObject;
        [self.tableView reloadData];
//        [self.teacherImage sd_setImageWithURL:[NSURL URLWithString:[responseObject objectForKey:@"headimg"]] placeholderImage:[UIImage imageNamed:@"老师"]];
//        self.teacherName.text = [responseObject objectForKey:@"name"];
//        self.teacherSex.text = [responseObject objectForKey:@"sex"];
//        self.teacherAcademic.text = [responseObject objectForKey:@"education"];
//        self.teacherPhone.text = [responseObject objectForKey:@"phone"];
//        self.teacherDate.text = [responseObject objectForKey:@"birthday"];
//        self.teacherCertificate.text = [responseObject objectForKey:@"have_cert"];
//        self.teacherAge.text = [responseObject objectForKey:@"work_time"];
//        self.teacherSchool.text = [responseObject objectForKey:@"campus_name"];
//        self.teacherAddress.text = [responseObject objectForKey:@"address"];
    }failure:^(id errorObject) {
        
    }];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 520;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
