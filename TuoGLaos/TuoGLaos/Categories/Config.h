//
//  Config.h
//  DDProcject_01
//
//  Created by iOS on 16/11/16.
//  Copyright © 2016年 IOS. All rights reserved.
//

#ifndef Config_h
#define Config_h
#pragma mark - DEBUG
#ifdef DEBUG
// 定义是输出Log
#define DLog(format, ...) NSLog(@"Line[%d] %s " format, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#else
// 定义是输出Log
#define DLog(format, ...)
#endif
// 只输出类名
#define LogClassName DLog(@"")

#pragma mark - 屏幕
//#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
//#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
//#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
//#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
//#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//#define isPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define kScrrenBounds [[UIScreen mainScreen] bounds]

#define kTabBarHeight 50

#define kNavAndStatusHeight 64


#pragma mark - 系统相关
// 检查系统版本
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

// 获取版本
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion [[UIDevice currentDevice] systemVersion]

//获取当前语言
#define CurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])


#pragma mark - 定义弱引用、强引用

#define WeakSelf(weakSelf)  __weak __typeof(&*self)weakSelf = self

#define ESWeak(var, weakVar) __weak __typeof(&*var) weakVar = var
#define ESStrong_DoNotCheckNil(weakVar, _var) __typeof(&*weakVar) _var = weakVar
#define ESStrong(weakVar, _var) ESStrong_DoNotCheckNil(weakVar, _var); if (!_var) return;

#define ESWeak_(var) ESWeak(var, weak_##var);
#define ESStrong_(var) ESStrong(weak_##var, _##var);

/** defines a weak `self` named `weakSelf` */
#define ESWeakSelf      ESWeak(self, weakSelf);
/** defines a strong `self` named `strongSelf` from `weakSelf` */
#define ESStrongSelf    ESStrong(weakSelf, strongSelf);

#define DLOG_CLASS_RELEASE_METHOD     NSLog(@"--%@ release---",[self class]);


#pragma mark - 定义警告宏
#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

#pragma mark - 其他
#define REAL_LEN(W)	(kScreenWidth*(W)/1242)
#define REAL_LEN_750(W)	(kScreenWidth*(W)/750)
#define REAL_LEN_375(W)	(kScreenWidth*(W)/375)
#define REAL_FONTSIZE(S) [UIFont systemFontOfSize:REAL_LEN(S)]


//G－C－D
#define BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define MAIN(block) dispatch_async(dispatch_get_main_queue(), block)


//颜色
#define kRGBColor(r, g, b)     [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define kRGBAColor(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(r)/255.0 blue:(r)/255.0 alpha:a]
#define kRandomColor  KRGBColor(arc4random_uniform(256)/255.0,arc4random_uniform(256)/255.0,arc4random_uniform(256)/255.0)

#define kColorWithHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]


// 检测block是否可用
#define BLOCK_EXEC(block, ...) if (block) { block(__VA_ARGS__); }

#pragma mark -----------

#define IOS9_OR_LATER  ( [[UIDevice currentDevice] systemVersion].floatValue >= 9.0 )
#define IOS8_OR_LATER  ( [[UIDevice currentDevice] systemVersion].floatValue >= 8.0 )
#define IOS7_OR_LATER  ( [[UIDevice currentDevice] systemVersion].floatValue >= 7.0 )
#define IOS6_OR_LATER  ( [[UIDevice currentDevice] systemVersion].floatValue >= 6.0 )
#define IOS5_OR_LATER  ( [[UIDevice currentDevice] systemVersion].floatValue >= 5.0 )
#define IOS4_OR_LATER  ( [[UIDevice currentDevice] systemVersion].floatValue >= 4.0 )
#define IOS3_OR_LATER  ( [[UIDevice currentDevice] systemVersion].floatValue >= 3.0 )

#define IOS8_OR_EARLIER !IOS9_OR_LATER
#define IOS7_OR_EARLIER !IOS8_OR_LATER
#define IOS6_OR_EARLIER !IOS7_OR_LATER
#define IOS5_OR_EARLIER !IOS6_OR_LATER
#define IOS4_OR_EARLIER !IOS5_OR_LATER
#define IOS3_OR_EARLIER !IOS4_OR_LATER

#define IS_SCREEN_3_INCH (CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size))
#define IS_SCREEN_4_INCH (CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size))
#define IS_SCREEN_55_INCH (CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size))
#define IS_SCREEN_47_INCH (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size))


#pragma mark -----------

#define IMAGE_SCALE_FACTOR (IS_SCREEN_55_INCH?3:2)

//宏定义单利
#undef	AS_SINGLETON
#define AS_SINGLETON( __class ) \
- (__class *)sharedInstance; \
+ (__class *)sharedInstance;
#undef	DEF_SINGLETON
#define DEF_SINGLETON( __class ) \
- (__class *)sharedInstance \
{ \
return [__class sharedInstance]; \
} \
+ (__class *)sharedInstance \
{ \
static dispatch_once_t once; \
static __class * __singleton__; \
dispatch_once( &once, ^{ __singleton__ = [[[self class] alloc] init]; } ); \
return __singleton__; \
}

//NSUserDefaults 实例化
#define USER_DEFAULT  [NSUserDefaults standardUserDefaults]

#define UserToken [USER_DEFAULT objectForKey:@"token"]

#

#pragma mark -------------判断是否为空
//字符串是否为空
#define kStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )
//数组是否为空
#define kArrayIsEmpty(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)
//字典是否为空
#define kDictIsEmpty(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)
//是否是空对象
#define kObjectIsEmpty(_object) (_object == nil \
|| [_object isKindOfClass:[NSNull class]] \
|| ([_object respondsToSelector:@selector(length)] && [(NSData *)_object length] == 0) \
|| ([_object respondsToSelector:@selector(count)] && [(NSArray *)_object count] == 0))

#endif /* Config_h */
