//
//  APP_SET_MACRO.h
//  XSDemo
//
//  Created by iOS on 17/1/23.
//  Copyright © 2017年 IOS. All rights reserved.


#ifndef APP_SET_MACRO_h
#define APP_SET_MACRO_h

#define DEFAULT_VIEWCOLOR UIColorHex(0x0CCE88)  //默认背景颜色
#define APPCOLOR [UIColor colorWithHexString:@"#44b356"]           //app主颜色
#define TEXT_DEFAULT_COLOR [UIColor colorWithHexString:@"#575757"] //默认黑色字体
#define TEXT_GRAY_COLOR [UIColor colorWithHexString:@"#A9A9A9"]     //深灰色字体/Users/juchao1/
#define TEXT_RED_COLOR [UIColor colorWithHexString:@"#CE5E58"]     //红色色字体

#define TEXT_LINE_BACKColor RGBCOLOR(235, 235, 235)     //红色色字体

#endif /* APP_SET_MACRO_h */
